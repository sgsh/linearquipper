{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE PatternSynonyms #-}

module MLL.Core where
import Prelude hiding((^), (*), (+))



--- MLL types -
newtype a -<> b = Lolli {unLolli :: a -> b}
infixr 0 -<>

newtype Bang a = Bang {unBang :: a}
pattern Tensor a b = (a, b)
type (a * b) = (,) a b
type One = ()

--- linear variable vid in Haskell context -
type LVar repr (vid::Nat) t =
  forall (v::Nat) (i::[Maybe Nat]) (o::[Maybe Nat]).
  Consume vid i o => repr v i o t

--- unrestricted variable in Haskell context -
type UVar repr t =
  forall (vid::Nat) (i::[Maybe Nat])
  . repr vid i i t

--- The syntax of MLL. -
class MLL (repr :: Nat -> [Maybe Nat] -> [Maybe Nat] -> * -> * ) where
  llam ::  (LVar repr vid t1 -> repr ('S vid) ('Just vid ': i) ('Nothing ': o) t2 ) -> repr vid i o (t1 -<> t2)
  (^) :: repr vid i h (t -<> t') -> repr vid h o t -> repr vid i o t'
  ulam :: (UVar repr t1 -> repr vid i o t2) -> repr vid i o (t1 -> t2)
  ($$) :: repr vid i o (t -> t') -> repr vid '[] '[] t -> repr vid i o t'
  bang :: repr vid '[] '[] t -> repr vid i i (Bang t)
  letBang :: repr vid i h (Bang t) -> (UVar repr t -> repr vid h o t') -> repr vid i o t'
  one :: repr vid i i One 
  letOne :: repr vid i h One -> repr vid h o t -> repr vid i o t
  (*) :: repr vid i h t1 -> repr vid h o t2 -> repr vid i o (t1 * t2)
  letStar :: repr vid i h (t1 * t2) ->
             (LVar repr vid t1 -> LVar repr ('S vid) t2 ->
              repr ('S ('S vid)) ('Just vid ': 'Just ('S vid) ': h) ('Nothing ': 'Nothing ': o) t) ->
             repr vid i o t
  letLV :: repr vid i h (t) -> (LVar repr vid t -> repr ('S vid) ('Just vid : h) ('Nothing : o) t') -> repr vid i o t'
  letUV :: repr vid '[] '[] (t) -> (UVar repr t -> repr (vid) (i) (o) t') -> repr vid i o t'
--------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------
data Nat = Z | S Nat

class Consume (v::Nat) (i::[Maybe Nat]) (o::[Maybe Nat]) | v i -> o
class Consume1 (b::Bool) (v::Nat) (x::Nat) (i::[Maybe Nat]) (o::[Maybe Nat]) | b v x i -> o
instance {-# OVERLAPS #-} (Consume v i o) => Consume v ('Nothing ': i) ('Nothing ': o)
instance {-# OVERLAPS #-} (EQ v x b, Consume1 b v x i o) => Consume v ('Just x ': i) o
instance {-# OVERLAPS #-} Consume1 'True v x i ('Nothing ': i)
instance {-# OVERLAPS #-} (Consume v i o) => Consume1 'False v x i ('Just x ': o)

class EQ (x::k) (y::k) (b::Bool) | x y -> b
instance {-# OVERLAPS #-} EQ x x 'True
instance {-# OVERLAPS #-} (b ~ 'False) => EQ x y b
--------------------------------------------------------------------------------------


  
--------------------------------------------------------------------------------------
newtype Ev (v::Nat) (i::[Maybe Nat]) (o::[Maybe Nat]) a = Ev {ev :: a}
instance MLL (Ev :: Nat -> [Maybe Nat] -> [Maybe Nat] -> * -> * ) where
  llam f = Ev $ Lolli $ \x -> ev (f (Ev x))
  f ^ x = Ev $ unLolli (ev f) (ev x)
  ulam f = Ev $ \x -> ev (f (Ev x))
  f $$ x = Ev $ ev f (ev x)
  x * y = Ev $ (,) (ev x) (ev y) 
  bang a = Ev $ Bang $ ev a 
  letBang a f = Ev $ (\a -> ev $ f $ Ev a) $ unBang $ ev a
  one = Ev ()
  letOne o (Ev a) =
    let _ = o in Ev a  
  letStar ab f =
    let Ev (a, b) = ab in
      Ev $ (\a b -> ev $ (f) (Ev a) (Ev b)) a b
  letLV e f =  Ev $ (\x -> ev $ f $ Ev x) $  ev e
  letUV e f =  Ev $ (\x -> ev $ f $ Ev x) $  ev e
--------------------------------------------------------------------------------------


    
--------------------------------------------------------------------------------------
(.*.) :: (MLL repr,
          Consume vid h1 ('Nothing ': o)) =>
         repr ('S vid) ('Just vid ': i) h2 (a2 -<> b) ->
         repr ('S vid) h2 h1 (a1 -<> a2) ->
         repr vid i o (a1 -<> b)
g .*. f = llam $ \a -> g ^ (f ^ a)

compose :: MLL repr =>
     repr ('S vid) '[] '[] (a1 -<> a2)
     -> repr ('S vid) '[] '[] (a2 -<> b)
     -> repr vid o o (a1 -<> b)
compose a b = llam $ \x ->  
  ((comp_ $$ b) $$ a) ^ x
  where
    comp_ = ulam $ \g -> ulam $ \f -> llam $ \x -> g ^ (f ^ x) 
--------------------------------------------------------------------------------------



--------------------------------------------------------------------------------------
class ToBang a b | a -> b where
  toBang :: a -> b
instance (ToBang a a', ToBang b b') => ToBang (a, b) (a' * b') where
  toBang (a, b) = (,) (toBang a)  (toBang b)
instance (ToBang a a', ToBang b b', ToBang c c') =>
         ToBang (a, b, c) (a' * b' * c') where
  toBang (a, b, c) = ((toBang a, toBang b) ,toBang c)
--------------------------------------------------------------------------------------


-----------------------------------------------------------------------------------------
type Defn t = forall repr i vid . (MLL repr) => repr vid i i t
defn :: Defn t -> Defn t
defn x = x
-----------------------------------------------------------------------------------------
