{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE PatternSynonyms #-}


module MLL.Convert where
import MLL.Core

--------------------------------------------------------------------------------------
class Corr a b | a -> b where
  to :: a -> b
  from :: b -> a
instance {-# OVERLAPS #-} (Corr a a', Corr b b') => Corr (a -<> b) (a' -> b') where
  to (Lolli f) = to . f . from
  from f = Lolli (from . f . to)
instance {-# OVERLAPS #-} (Corr a a', Corr b b') => Corr (a -> b) (a' -> b') where
  to (f) = to . f . from
  from f = (from . f . to)
instance {-# OVERLAPS #-} (Corr a a', Corr b b') => Corr (a * b) (a' , b') where
  to (a, b) = (to a, to b)
  from (a, b) = (,) (from a) (from b)
instance Corr () () where
  to = id
  from = id
instance {-# OVERLAPS #-} (Corr a b) => Corr (Bang a) b where
  to (Bang a) = to a
  from a = Bang (from a)
--------------------------------------------------------------------------------------
convertMono :: Corr a r => Ev v i o a -> r
convertMono = to . ev 

reconvertMono :: Corr a r => r -> Ev v i o a
reconvertMono = Ev . from
--------------------------------------------------------------------------------------

