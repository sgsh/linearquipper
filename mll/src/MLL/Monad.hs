{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE PatternSynonyms #-}

module MLL.Monad where
import Prelude hiding ((*),(+),(^))
import MLL.Core
import MLL.Pattern


------------------------------------------------------------------------------------------
class (MLL repr, Monad m) => MLL_Monad m repr where
  (>>=.) :: repr vid i h (m t) -> repr vid h o (t -<> m t') -> repr vid i o (m t')
  bind_ :: repr vid i h (m t) ->
           (LVar repr vid t -> repr ('S vid) ('Just vid : h) ('Nothing : o) (m t')) ->
           repr vid i o (m t')
  -- sbind :: repr vid i h (m t) ->
  --          (p -> repr vid' h' o' (m t')) ->
  --          repr vid i o (m t')
  -- sbind_ :: repr vid i h (m t) ->
  --          (p -> repr (vid) (h') (o') (m t')) ->
  --          repr vid i o (m t')
  return_ :: repr vid i o t -> repr vid i o (m t)
  (>>) :: repr vid i h (m One) ->
          repr ('S vid) ('Nothing : h) ('Nothing : o) (m t) ->
          repr vid i o (m t)
          
instance Monad m=> MLL_Monad m Ev where
  m >>=. funM = Ev $ ev m >>= (\a -> (unLolli $ ev funM) a)
  bind_ m funM = m >>=. llam funM
  -- sbind = letSSM --ma >>=.  (llam  $ \ a' -> letSS a' funM)
  -- sbind_ m funM = Ev $ ev m >>= (ev (ulam funM))
  return_ a = Ev $ return  $ ev a
  (>>) m1 m2 = m1 `bind_` \ x -> letOne x m2 
------------------------------------------------------------------------------------------



------------------------------------------------------------------------------------------
letSSM :: ( MLL_Monad m repr,
            TensP repr tpair pair ('S vid) vid2 ('Nothing : h) h2 ('Nothing : o) o2) =>
          repr vid i h (m tpair) ->
          (pair -> repr vid2 h2 o2 (m t')) ->
          repr vid i o (m t')
letSSM m k = m `bind_` \a -> letSS a k


-- class ( MLL repr,  Monad m , TensP repr tp p vid vid' h h' o o') =>
--         TensPM m repr tp p vid vid' h h' o o' -- |
-- --  tp repr vid -> p,  vid p -> vid', vid h p -> h', vid o p -> o'
--   where  
--     (.>>=.) :: repr vid i h (m tp) -> repr vid' h' o' (p -<> m t') -> repr vid i o (m t')
--     sbind :: repr vid i h (m tp) ->
--                (p -> repr vid' h' o' (m t')) ->
--                repr vid i o (m t')
--     letSSM :: 
-- instance ( MLL repr, Monad m, TensP repr tp p vid vid' h h' o o') => TensPM m repr tp p vid vid' h h' o o' where
--   -- ma .>>=. funM = Ev $ ev ma >>= (\a -> (unLolli $ ev funM) a)
--   sbind ma k = ma >>=. llam (\a -> letSSM a k)

  
-- sbind :: ( -- MLL m, MLL repr,
--            MLL_Monad m repr,
--            TensP repr tp p vid vid' h h' o o') =>
--           repr vid i h (m tp) ->
--           (p -> repr vid' h' o' (m t')) ->
--           repr vid i o (m t')
-- sbind m k = m >>=. (llam $  \a -> letSS a k)
------------------------------------------------------------------------------------------

