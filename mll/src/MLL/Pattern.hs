{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE PatternSynonyms #-}


module MLL.Pattern where
import Prelude hiding((^), (*), (+))
import MLL.Core


newtype UV (repr :: Nat -> [Maybe Nat] -> [Maybe Nat] -> * -> *) a = 
  UV {unUV :: UVar repr a}
newtype LV (repr :: Nat -> [Maybe Nat] -> [Maybe Nat] -> * -> *) (vid :: Nat) a = 
  LV {unLV ::LVar repr vid a}


------------------------------------------------------------------------------------------------------
class MLL repr =>
      TensP (repr :: Nat -> [Maybe Nat] -> [Maybe Nat] -> * -> * ) tpair pair vid vid' h h' o o' |
      tpair repr vid -> pair, vid pair -> vid', vid h pair -> h', vid o pair -> o'--,  pair repr vid -> tpair
      where
        letSS :: repr vid i h tpair -> (pair -> repr vid' h' o' t) -> repr vid i o t
-- instance MLL repr =>
--          TensP repr t (LV repr vid t) vid (S vid) h (Just vid:h) o (Nothing:o) where
--   letSS e f = letLV e $ \e' -> f (LV e')
instance ( MLL repr,
           TensP repr (t1) pair1 ('S ('S vid)) vid''
                 ('Nothing : 'Just ('S vid) : h) h'' ('Nothing : 'Nothing : o)  o'',
           TensP repr (t2) pair2 vid'' vid' h''' h' o'' o'
         ,Consume ('S vid) h'' h'''
         ) =>
         TensP repr (t1*t2) (pair1,pair2) vid vid' h h' o o'
         where
           letSS abcd f = letStar abcd $ \ ab cd -> letSS ab $ \ ab -> letSS cd $ \cd -> f (ab, cd)
instance MLL repr  =>
         TensP repr (Bang t) (UV repr t) vid vid h h o o
         where
           letSS b f = letBang b $ \ b'  -> f (UV b')              
instance ( MLL repr, 
           TensP repr (Bang t) (UV repr pair) vid vid' i h' o o') =>
         TensP repr (Bang (Bang t)) (UV repr pair) vid vid' i h' o o'
         where
           letSS b f = letBang b $ \ b' -> letSS b' $ \ b'' -> f b''                
-- instance ( TensP repr  tp1  a ('S ('S vid)) vid'1 ('Nothing : 'Just ('S vid) : h) h'1 ('Nothing : 'Nothing : o) o'1,
--            TensP repr tp2 b vid'1 vid' h2 h' o'1 o',
--            Consume ('S vid) h'1 h2) =>
--          TensP repr (Bang (tp1 * tp2)) ((a,b)) vid vid' h h' o o' 
--          where
--            letSS ab f = letBang ab $ \ ab' -> letStar ab' $ \ a b -> letSS a $ \ a' -> letSS b $ \ b' -> f (a', b')
instance MLL repr => TensP repr One () vid vid h h o o 
         where
           letSS o f = letOne o $ f ()           
instance (MLL repr)=>
         TensP repr  ((t1 -<> t2)) (LV repr vid (t1 -<> t2)) (vid) (S vid) h (Just vid : h) o (Nothing : o)
         where
           letSS e f = letLV e $ \ e' -> f (LV e')
instance (MLL repr)=>
         TensP repr  ((t1 -> t2)) (LV repr vid (t1 -> t2)) (vid) (S vid) h (Just vid :h) o (Nothing : o)
         where
           letSS e f = letLV e $ \ e' -> f (LV e')  
                                         
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------


-- (@*) ::
--     (Consume ('S vid) i2 o2, Consume vid i1 o1, Or b1 b2 b,
--       VarOk b2 var1, VarOk b2 var0, LLC repr) =>
--      (repr v1 'False i1 o1 a1
--       -> (a2 -> t1)
--       -> repr ('S ('S vid)) b2 ('Just vid ': 'Just ('S vid) ': h) (var0 ': var1 ': o3) c)
--      -> (repr v2 'False i2 o2 d1 -> (d2 -> t2) -> t1)
--      -> repr vid b1 i3 h (a1 * d1)
--      -> ((a2, d2) -> t2)
--      -> repr vid b i3 o3 c
-- (@*) p1 p2 z k =
--   letStar z $ \a b ->
--                 p1 a (\x->
--                          p2 b (\y -> k (x, y)))
-- pvar ::
--     (LLC repr, Or tf1 tf2 tf, VarOk tf1 var') =>
--     repr vid tf2 h o a -> 
--     (LVar repr vid a -> repr ('S vid) tf1 ('Just vid ': i) (var' ': h) b) ->
--     repr vid tf i o b
-- pvar a k = llam k ^ a

-- pbang ::
--   (LLC repr, Or b1 b2 b) =>
--   repr vid1 b1 i1 h (Bang t1) ->
--   (repr vid2 'False i2 i2 t1 -> repr vid1 b2 h o t2) ->
--   repr vid1 b i1 o t2
-- pbang x k = letBang x $ \x -> k x 
  
-- letQ :: t1 -> (t1 -> t2 -> t3) -> t2 -> t3
-- letQ e p k = p e k 

-- -------------------------------------------
