-- | The \"@superdoc-armor@\" preprocessor. It converts all UTF8 codes
-- to ASCII armor.  See "Distribution.Superdoc.Markup" for a
-- description of the armored format.

module Main where

import System.Environment
import System.Exit
import System.IO

import Distribution.Superdoc.UTF8
import Distribution.Superdoc.Markup

-- | A Haskell preprocessor that converts UTF8 to ASCII armor. 
-- 
-- Like all Haskell preprocessors, this program expects three
-- arguments: /source/, /infile/, /outfile/. Here, /source/ is the
-- name of the original source file (in case a preprocessor wants to
-- insert LINE pragmas - this one doesn't do so), and /infile/ and
-- /outfile/ are the files to read from and write to, respectively.
-- 
-- As an alternative, we also allow the flag \"-f\", in which case the
-- preprocessor runs as a filter (from 'stdin' to 'stdout'). This is
-- sometimes convenient for testing.
main :: IO ()
main = do
  args <- getArgs
  case args of
    [source, infile, outfile] -> do
      filter_file (filter_id . to_armor . parse_utf8) infile outfile
    ["-f"] -> do
      filter_handles (filter_id . to_armor . parse_utf8) stdin stdout
    ["-h"] -> do
      preproc_usage stdout
      exitSuccess
    ["--help"] -> do
      preproc_usage stdout
      exitSuccess
    _ -> do
      preproc_usage stderr
      exitFailure

-- | Print a usage message for the preprocessor.
preproc_usage :: Handle -> IO ()
preproc_usage h = do
  name <- getProgName
  hPutStrLn h ("Usage: " ++ name ++ " <source> <infile> <outfile>")
  hPutStrLn h ("       " ++ name ++ " -f")
  hPutStrLn h ("Arguments:")
  hPutStrLn h (" <source>   -  The name of the original source file")
  hPutStrLn h (" <infile>   -  The file to read")
  hPutStrLn h (" <outfile>  -  The file to write to")
  hPutStrLn h ("Options:")
  hPutStrLn h (" -h, --help -  Print this usage message and exit")
  hPutStrLn h (" -f         -  Run as a filter from stdin to stdout (for testing)")
