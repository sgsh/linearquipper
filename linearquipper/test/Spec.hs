{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE PatternSynonyms #-}
-- {-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE GADTs #-}

module Main (main) where
import Test.Hspec
import qualified Control.Monad as M

import Prelude hiding ((*),(+),(^),(>>=),return,(>>))
import MLL.Core
import MLL.Pattern
import MLL.Convert
import MLL.Monad hiding ((>>))
import Quipper (Circ, Qubit,Bit)
import LinearQuipper
import LinearQuipper.MLL
import QuipperLib.Simulation (sim_generic)
----------------------------------------------------------------------------
(>>=) = bind_ 
return = return_
(>>) m1 m2 = m1 >>= \x -> letOne x m2 
----------------------------------------------------------------------------
-- temlete //

-- spec :: Spec
-- spec =
--   describe "sim_generic (convert teleport)" $ do
--   it "True" $ do
--     sim_teleport1 True `shouldBe` [(True, 1.0)]
--   it "False" $ do
--     sim_teleport1 False `shouldBe` [(False, 1.0)]
--   where
--     (>>=) = (M.>>=)
--     return =  M.return

main :: IO ()
main =
  -- Test.defaultMain (Test.hUnitTestToTests ([(True,1.0)] ~=? sim_teleport1 True))
  putStrLn "Test suite not yet implemented"

----------------------------------------------------------------------------
plus_minus :: (LinearQuipper repr) =>
  UVar repr (Bool -> Circ Qubit)
plus_minus = qdefn $ ulam $ \b -> do
  q <- qinit $$ b 
  hadamard ^ q 

-- simpletest :: (LinearQuipper repr) => UVar repr (Qubit -<> Circ Qubit)
-- simpletest = {-qdefn $-} llam $ \q -> (qdefn hadamard) ^ q

-- simpletest2 :: (LinearQuipper repr) => UVar repr (Qubit -<> Circ Qubit)
-- simpletest2 = qdefn hadamard

share :: LinearQuipper repr =>
  UVar repr (Qubit -<> Circ (Qubit * Qubit))
share = qdefn $llam $ \a -> do
  b <- qinit $$ false
  b <- qnot ^ b `controlled` a
  return (a * b)  

bell00 :: LinearQuipper repr =>
  UVar repr (Circ(Qubit * Qubit))
bell00 = qdefn $ do
  a <- plus_minus $$ false
  share ^ a
  
alice :: LinearQuipper repr =>
  UVar repr (Qubit -<> Qubit -<> Circ (Bang Bit * Bang Bit))
alice = qdefn $ llam $ \q -> llam $ \a -> do
  a <- qnot ^ a `controlled` q
  q1 <- hadamard ^ q
  measure ^ (q1 * a)
  
bob :: LinearQuipper repr =>
  UVar repr (Qubit -<> (Bit -> Bit -> Circ Qubit))
bob = qdefn $llam $ \b -> ulam $ \x -> ulam $ \y -> do
  b <- gate_X ^ b `controlled` y
  b <- gate_Z ^ b `controlled` x
  cdiscard $$ (x * y)  
  return b

-- teleport_letS :: LinearQuipper repr => UVar repr (Qubit -<> Circ(Qubit))
-- teleport_letS = llam $ \q -> do
--   ab <- bell00
--   b <- letStar ab $ \a b -> do
--     xy <- alice ^ q ^ a
--     b <- letStar xy $ \x y -> do
--       b <- letBang x $ \x -> letBang y $ \y -> bob ^ b $$ x $$ y
--       return b
--     return b
--   return b

-- teleport :: LinearQuipper repr => UVar repr (Qubit -<> Circ Qubit)
-- teleport =  qdefn $ llam $ \q -> do
--   ab <- bell00
--   letQ ab (pvar @* pvar) $ \(a, b) -> do
--     xy <- alice ^ q ^ a
--     letQ xy (pbang @* pbang)$ \(x, y) ->
--       bob ^ b $$ x $$ y

-- teleportLetQM :: LinearQuipper repr =>
--   UVar repr (Qubit -<> Circ Qubit)
-- teleportLetQM = qdefn $ llam $ \q ->
--   letQM bell00 (pvar @* pvar) $ \(a, b) ->
--   letQM (alice ^ q ^ a) (pbang @* pbang) $ \(x, y) ->
--   bob ^ b $$ x $$ y

teleportLetSS :: LinearQuipper repr =>
  UVar repr (Qubit -<> Circ Qubit)
teleportLetSS = qdefn $ llam $ \q ->
  letSSM bell00 $ \(LV a, LV b) ->
  letSSM (alice ^ q ^ a) $ \(UV x, UV y) ->
  bob ^ b $$ x $$ y

-- letSSM m k = m `bind_` \a -> letSS a k
----------------------------------------------------------------------------



----------------------------------------------------------------------------
-- check1 :: LinearQuipper repr =>
--   UVar repr (Circ (Qubit * Qubit))
-- check1 = {-qdefn $-} do
--   a <- qinit $$ false
--   pair <- share ^ a
--   letStar pair $ \a b -> do
--     aa <- hadamard ^ a
--     bb <- hadamard ^ b
--     return (aa * bb)


-- check2 :: LinearQuipper repr =>
--   UVar repr (Qubit -<> Qubit -<> Circ Qubit)
-- check2 = {-qdefn $-} llam $ \q1 -> llam $ \q2 -> do
--   q1 <- measure ^ q1
--   letBang q1 $ \q1 -> cdiscard $$ q1
--   return q2
----------------------------------------------------------------------------



  
mycirc :: LinearQuipper repr =>
  UVar repr (Qubit -<> Qubit -<> Circ(Qubit * Qubit)) 
mycirc = qdefn $ llam $ \a -> llam $ \b -> do
  a <- hadamard ^ a
  b <- hadamard ^ b
  a <- controlled (qnot ^ a) b
  return (a * b)

-- mycirc2 ::LinearQuipper repr => UVar repr (Qubit -<> Qubit -<> Qubit -<> Circ(Qubit * Qubit * Qubit))
-- mycirc2 = llam $ \a -> llam $ \b -> llam $ \c -> do
--   ab <- mycirc ^ a ^ b
--   ab <- with_controls c $ letStar ab $ \a b -> do
--     ab <- mycirc ^ a ^ b
--     ab <- letStar ab $ \a b -> do
--       mycirc ^ b ^ a
--     return ab  
--   abc <- letStar ab $ \a b -> do
--     ac <- mycirc ^ a ^ c
--     abc <- letStar ac $ \a c -> do
--       return (a * b * c)
--     return abc
--   return abc
--   where
--     (>>=) = bind_
--     (>>) m1 m2 = m1 >>= \x -> letOne x m2 
--     return = return_
-- print_mycirc2 format = Q.print_simple format $ (coerce $ ev mycirc2 :: Qubit -> Qubit -> Qubit -> Circ (Qubit * Qubit * Qubit))
  
-- mycirc3 :: LinearQuipper repr => UVar repr (Qubit -<> Qubit -<> Qubit -<> Circ(Qubit * Qubit * Qubit))
-- mycirc3 = llam $ \a -> llam $ \b -> llam $ \c -> do
--   abc <- with_ancilla (\x -> do
--     x <- controlled (qnot ^ x) (a * b)
--     c <- controlled (hadamard ^ c) (a * b)
--     x <- controlled (qnot ^ x) (a * b)
--     return ((a * b * c) * x))
--   return abc
--   where
--     (>>=) = bind_
--     (>>) m1 m2 = m1 >>= \x -> letOne x m2 
--     return = return_
--print_mycirc3 format = Q.print_simple format (coerce (ev mycirc3) :: Qubit -> Qubit -> Qubit -> Circ(Qubit * Qubit * Qubit))
--toControlSource :: LinearQuipper repr => repr v1 tf1 i1 o1 a -> repr v2 tf2 i2 o2 b -> repr v3 tf3 i3 o3 (a, b) 
--toControlSource a b = Ev (ev a, ev b)

--print_simple format qfun = Quipper.print_simple (coerce $ ev qfun)
mycirc4 :: LinearQuipper repr =>
  UVar repr (Qubit -<> Qubit -<> Qubit -<> Circ(Qubit * Qubit * Qubit))
mycirc4 = {-qdefn $-} llam $ \a -> llam $ \b -> llam $ \c -> do
  c <- qnot ^ c `controlled` (a * b)
  return (a*b*c)

test4 :: LinearQuipper repr =>
  UVar repr (Circ(Qubit * Qubit * Qubit))
test4 = {-qdefn $-} do
  a <- qinit $$ true
  b <- qinit $$ true
  c <- qinit $$ false
  mycirc4 ^ a ^ b ^ c

-- timestep :: LinearQuipper repr => UVar repr (Qubit -<> Qubit -<> Qubit -<> Circ(Qubit * Qubit * Qubit))
-- timestep = llam $ \a -> llam $ \b -> llam $ \c -> do
--   ab <- mycirc ^ a ^ b
--   abc <- letStar ab $ \a b -> do
--     c <- controlled2 (qnot ^ c) a b
--     ab <- reverse_simple2 $$ mycirc ^ a ^ b
--     return $ letStar ab $ \a b -> (a * b * c)
--   return abc
--   where
--     (>>=) = bind_
--     (>>) m1 m2 = m1 >>= \x -> letOne x m2 
--     return = return_
-- print_timestep format = Q.print_simple format $ (coerce $ ev timestep :: Qubit -> Qubit -> Qubit -> Circ (Qubit * Qubit * Qubit))   

-- timestep2 :: LinearQuipper repr => UVar repr (Qubit -<> Qubit -<> Qubit -<> Circ(Qubit * Qubit * Qubit))
-- timestep2 = 
--   undefined








-- class LLC_M m repr => Bind m repr a where
--   type FuncType m repr a :: Nat -> Bool -> [Maybe Nat] -> [Maybe Nat] -> [Maybe Nat] -> * -> *
--   smartBind :: m (repr vid tf i j a) -> FuncType m repr a vid tf2 i j k b 

-- instance {-# OVERLAPPING #-} LLC_M m repr => Bind m repr a where
--   type FuncType m repr a vid tf1 i j k b = -- forall tf2 tf var. 
--     (Or tf1 tf2 tf, VarOk tf2 var) =>
--     (LVar repr vid a -> repr (S vid) tf2 (Just vid : j) (var : k) (m b)) ->
--     repr vid tf i j (m b) 

--   smartBind = bind_
  
-- try
-- data Tensor' repr vid tf i o a b = forall tf1 tf2 h. Or tf1 tf2 tf => Tensor' (repr vid tf1 i h a) (repr vid tf2 h o b)
-- tensor' :: LLC repr => repr vid tf i o (a * b) -> Tensor' repr vid tf i o a b
-- tensor' ab = undefined
-- untensor' :: LLC repr => Tensor' repr vid tf i o a b -> repr vid tf i o (a * b)
-- untensor' (Tensor' a b) = a * b
-- pattern Tens a b <- (tensor' -> Tensor' a b)
--  where Tens a b = untensor' (Tensor' a b)

-- data Tensor' repr vid tf i o a b = forall tf1 tf2 h. Or tf1 tf2 tf => Tensor' (repr vid tf1 i h a) (repr vid tf2 h o b)
-- tensor' :: LLC repr => repr vid tf i o ( (a * b)) -> Tensor' repr vid tf i o ( a) ( b)
-- tensor' ab = undefined
-- untensor' :: LLC repr => Tensor' repr vid tf i o a b -> repr vid tf i o (a * b)
-- untensor' (Tensor' a b) = a * b
-- pattern Tens a b <- (tensor' -> Tensor' a b)

-- data UorL repr vid tfu iu ou tfl il ol a b =
--    UorL (repr vid tfu iu ou a) (repr vid tfl il ol b)



--------------------------------------------------------------------------------------
-- mycnot :: LinearQuipper repr =>
--   UVar repr (Qubit -<> Qubit -<> Circ(Qubit * Qubit))
-- mycnot = llam $ \a -> do
--   a <- qnot ^ a `controlled` c
--   return (a * c)

myhadcnot :: LinearQuipper repr =>
  UVar repr (Qubit -<> Circ(Bang Bit * Bang Bit))
myhadcnot = qdefn $ llam $ \a -> do
  c <- qinit $$ false
  c <- hadamard ^ c
  a <- qnot ^ a `controlled` c
  measure ^ (a * c)

-- qnothad :: LinearQuipper repr =>
--   UVar repr (Qubit -<> Circ (Qubit))
-- qnothad = {-qdefn $-} llam $ \q -> do
--   q' <- qnot ^ q
--   q'' <- hadamard ^ q'
--   return q''

    
-- ccnot :: LinearQuipper repr => UVar repr (Qubit -<> Qubit -<> Qubit -<> Circ(Qubit * Qubit *  Qubit))
-- ccnot =  llam $ \c -> llam $ \a -> llam $ \b -> do
--   c <- qnot ^ c `controlled` (a * b)
--   return ((c * a) * b)

-- nand :: LinearQuipper repr => UVar repr (Qubit -<> Qubit -<> Circ(Qubit * Qubit * Qubit))
-- nand = llam $ \a -> llam $ \b -> do
--   c <- qinit $$ false
--   cab <- ccnot ^ c ^ a ^ b
--   letStar cab $ \c ab ->    
--     letStar ab $ \a b -> do
--     c <- qnot ^ c
--     return (a * b * c)

-- mycirc5 :: LinearQuipper repr => UVar repr (Qubit -<> Qubit -<> Circ (Qubit * Qubit))
-- mycirc5 = llam $ \a -> llam $ \b -> do
--   ab <- (reverse_simple $$ mycirc) ^ (a * b)
--   return ab
    
-- hadqnot :: LinearQuipper repr => UVar repr (Qubit -<> Circ Qubit)
-- hadqnot = llam $ \q -> do
--   q <- hadamard ^ q
--   q <- qnot ^ q
--   return q

-- apphadnot = ulam $ \b -> do
--   q <- qinit $$ b
--   q <- simplehadnot ^ q
--   return q

-- badcirc :: LinearQuipper repr =>
--   UVar repr (Qubit -<> Qubit -<> Circ Qubit)
-- badcirc = {-qdefn $-} llam $ \q1 -> llam $ \q2 -> do
--   q' <- measure ^ q1
--   q'' <- hadamard ^ q1
--   return q2
--   where
--     (>>=) = bind_ 
--     return = return_

-- badcirc' :: LinearQuipper repr =>
--   UVar repr (Qubit -<> Qubit -<> Circ Qubit)
-- badcirc' = {-qdefn $-} llam $ \q1 -> llam $ \q2 -> let (>>=) = bind_ in let return = return_ in do{ q' <- measure ^ q1; q'' <- hadamard ^ q1; return q2}


-- badcirc2 :: UVar repr (Qubit -<> Circ Qubit)
-- badcirc2 = {-qdefn $-} llam $ \q -> (qnot ^ q) `controlled` q
  
timestep :: LinearQuipper repr => UVar repr (Qubit -<> Qubit -<> Qubit -<> Circ (Qubit * Qubit * Qubit))
timestep = llam $ \a -> llam $ \b -> llam $ \c -> do 
  ab <- mycirc ^ a ^ b
  c' <- (qnot ^ c) `controlled` ab
  ab' <- reverse_simple $$ mycirc ^ ab
  return (ab' * c')

-- sim_teleport1 b =
--   sim_generic 0.1234 (convert teleport) b
