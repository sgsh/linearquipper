{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE UndecidableInstances #-}


module LinearQuipper.MLL where
import Prelude hiding ((*),(+),(^))
import MLL.Core
import MLL.Convert
import MLL.Pattern
import qualified Quipper as Q
import Quipper (Bit, Qubit, Circ())
import LinearQuipper


--------------------------------------------------------------------------------
instance ToBang Bit (Bang Bit) where
  toBang = Bang 
instance ToBang Bool (Bang Bool) where
  toBang = Bang
--------------------------------------------------------------------------------



--------------------------------------------------------------------------------
instance Corr Bool Bool where
  from = id
  to = id
instance Corr Qubit Qubit where
  from = id
  to =id
instance Corr Bit Bit where
  from = id
  to = id

instance Corr a b => Corr (Circ a) (Circ b) where
  from = fmap from
  to = fmap to 
--------------------------------------------------------------------------------
convert :: Corr a r => (forall repr. LinearQuipper repr => repr v i o a) -> r
convert = convertMono
--------------------------------------------------------------------------------



--------------------------------------------------------------------------------
instance (MLL repr)=>
         TensP repr (Qubit) (LV repr vid Qubit) (vid) ('S vid) h ('Just vid : h) o ('Nothing : o)
         where
           letSS e f = letLV e $ \ e' -> f (LV e')
instance (MLL repr)=>
         TensP repr (Bit) (LV repr vid Bit) (vid) ('S vid) h ('Just vid : h) o ('Nothing : o)
         where
           letSS e f = letLV e $ \ e' -> f (LV e')
instance (MLL repr)=>
         TensP repr (Bool) (LV repr vid Bool) (vid) ('S vid) h ('Just vid : h) o ('Nothing : o)
         where
           letSS e f = letLV e $ \ e' -> f (LV e')

-- instance MLL repr =>
--          TensP repr t (LV repr vid t) vid (S vid) h (Just vid:h) o (Nothing:o) where
--   letSS e f = letLV e $ \e' -> f (LV e')



-- instance (MLL repr)=>
--          TensP repr (Bang Bit) (UV repr Bit) (vid) (vid) h (h) o (o)
--          where
--            letSS e f = letBang e $ \ e' -> f (UV e') 
-- instance (MLL repr)=>
--          TensP repr (Bang Bool) (UV repr Bool) (vid) (vid) h (h) o (o)
--          where
--            letSS e f = letBang e $ \ e' -> f (UV e')
-- instance (MLL repr)=>
--          TensP repr (Bang (t1 -<> t2)) (UV repr (t1 -<> t2)) (vid) (vid) h (h) o (o)
--          where
--            letSS e f = letBang e $ \ e' -> f (UV e')
-- instance (MLL repr)=>
--          TensP repr (Bang (t1 -> t2)) (UV repr (t1 -> t2)) (vid)  vid h (h) o (o)
--          where
--            letSS e f = letBang e $ \ e' -> f (UV e')



-- instance MLL repr =>
--          TensP repr (Bang t) (UV repr t) (vid) vid h h o o
--          where
--            letSS e f = letBang e $ \ e' -> f (UV e')     
           
--------------------------------------------------------------------------------
