{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE UndecidableInstances #-}


module LinearQuipper where
import Data.Coerce
import Prelude hiding ((^), (*), (+))
import MLL.Core
import MLL.Monad
import Quipper
import Quipper.Generic
import Quipper.Monad (reverse_encapsulated)

  
class (MLL_Monad Circ repr) => LinearQuipper repr  where

  true :: UVar repr Bool
  false :: UVar repr Bool
  -- * Basic types
  -- Qubit
  -- Bit
  -- Qulist
  -- Bitlist
  
  -- * Basic gates
  -- $BASIC
  -- Timestep
  
  -- $FUNCTIONAL_ANCHOR
  
  -- ** Reversible gates in functional style
  -- $FUNCTIONAL
  qnot :: UVar repr (Qubit -<> Circ Qubit)
  hadamard :: UVar repr (Qubit -<> Circ Qubit)
  gate_H :: UVar repr (Qubit -<> Circ Qubit)
  gate_X :: UVar repr (Qubit -<> Circ Qubit)
  gate_Y :: UVar repr (Qubit -<> Circ Qubit)
  gate_Z :: UVar repr (Qubit -<> Circ Qubit)
  gate_S :: UVar repr (Qubit -<> Circ Qubit)
  gate_S_inv :: UVar repr (Qubit -<> Circ Qubit)
  gate_T :: UVar repr (Qubit -<> Circ Qubit)
  gate_T_inv :: UVar repr (Qubit -<> Circ Qubit)
  gate_E :: UVar repr (Qubit -<> Circ Qubit)
  gate_E_inv :: UVar repr (Qubit -<> Circ Qubit)
  gate_omega :: UVar repr (Qubit -<> Circ Qubit)
  gate_V :: UVar repr (Qubit -<> Circ Qubit)
  gate_V_inv :: UVar repr (Qubit -<> Circ Qubit)
  expZt :: UVar repr (Timestep -> Qubit -<> Circ Qubit)
  rGate :: UVar repr (Int -> Qubit -<> Circ Qubit)
  gate_W :: UVar repr (Qubit -<> Qubit -<> Circ (Qubit * Qubit))
  gate_iX :: UVar repr (Qubit -<> Circ Qubit)
  gate_iX_inv :: UVar repr (Qubit -<> Circ Qubit)
  global_phase :: UVar repr (Double -> Circ ())
  global_phase_anchored :: QCData qc => UVar repr (Double -> qc -<> Circ ())
  qmultinot :: QData qa => UVar repr (qa -<> Circ qa)
  cnot :: UVar repr (Bit -> Circ Bit)
  swap :: QCData qc => UVar repr (qc -<> qc -<> Circ (qc * qc))
  
  -- $IMPERATIVE_ANCHOR
  
  -- ** Reversible gates in imperative style 
  -- $IMPERATIVE 
  -- qnot_at ::
  -- hadamard_at :: 
  -- gate_H_at :: 
  -- gate_X_at :: 
  -- gate_Y_at :: 
  -- gate_Z_at :: 
  -- gate_S_at :: 
  -- gate_S_inv_at :: 
  -- gate_T_at :: 
  -- gate_T_inv_at :: 
  -- gate_E_at :: 
  -- gate_E_inv_at :: 
  -- gate_omega_at :: 
  -- gate_V_at :: 
  -- gate_V_inv_at :: 
  -- expZt_at :: 
  -- rGate_at :: 
  -- gate_W_at :: 
  -- gate_iX_at :: 
  -- gate_iX_inv_at :: 
  -- qmultinot_at :: 
  -- cnot_at :: 
  -- swap_at :: 
    
  -- ** Gates for state preparation and termination
  qinit :: QShape ba qa ca => UVar repr ( ba -> Circ qa )
  qterm :: QShape ba qa ca => UVar repr (ba -> qa -<> Circ One)
  -- qdiscard :: QData qa => UVar repr (qa -<> Circ ())
  cinit :: QShape ba qa ca => UVar repr (ba -> Circ ca)
  cterm :: QShape ba qa ca => UVar repr (ba -> ca -> Circ ())
  cdiscard :: CData ca => UVar repr (ca -> Circ One)
  -- qc_init :: 
  -- qc_init_with_shape :: 
  -- qc_term :: 
  -- qc_discard :: 
  measure :: (QShape ba qa ca, ToBang ca ca') => 
             UVar repr (qa -<> Circ ca')
  -- prepare :: 
  -- qc_measure :: 
  -- qc_prepare :: 
    
  -- ** Gates for classical circuits
  -- $CLASSICAL
  cgate_xor :: UVar repr ([Bit] -> Circ Bit)
  cgate_eq :: UVar repr ( Bit -> Bit -> Circ Bit)
  cgate_not :: UVar repr (Bit -> Circ Bit)
  cgate_and :: UVar repr ( Bit -> Bit -> Circ Bit) 
  cgate_or :: UVar repr ( Bit -> Bit -> Circ Bit)
  cgate_if :: CData ca => UVar repr (Bit -> ca -> ca -> Circ ca)
  circ_if :: CData ca => UVar repr (Bit -> Circ ca -> Circ ca -> Circ ca)
    
  -- ** User-defined gates
  -- named_gate :: 
  -- named_gate_at :: 
  -- named_rotation :: 
  -- named_rotation_at :: 
  -- extended_named_gate :: 
  -- extended_named_gate_at :: 
    
  -- ** Dynamic lifting
  -- dynamic_lift :: 
    
  -- * Other circuit-building functions
  -- qinit_plusminus :: 
  -- qinit_of_char :: 
  -- qinit_of_string :: 
  map_hadamard :: QData qa => UVar repr (qa -<> Circ qa)
  -- map_hadamard_at :: 
  -- controlled_not :: ControllSource c => UVar repr (a -<> Circ a -<> Circ a)
  -- controlled_not_at :: 
  -- bool_controlled_not :: 
  -- bool_controlled_not_at :: 
  -- qc_copy :: 
  -- qc_uncopy :: 
  -- qc_copy_fun :: 
  -- qc_uncopy_fun :: 
  -- mapUnary :: 
  -- mapBinary :: 
  -- mapBinary_c :: 
  -- qc_mapBinary :: 
    
  -- * Notation for controls
  -- $CONTROLS
  -- ControlSource(..) :: 
  -- ControlList :: 
  -- (.&&.) ::  
  -- (.==.) ::  
  -- (./=.) :: 
  controlled :: ControlSource c => repr vid i h (Circ a) -> repr vid h o c -> repr vid i h (Circ a)

    
  -- * Signed items
  -- Signed(..) :: 
  -- from_signed :: 
  -- get_sign :: 
    
  -- * Comments and labelling
  -- comment :: 
  -- label :: 
  -- comment_with_label :: 
  -- without_comments :: 
  -- Labelable :: 
    
  -- * Hierarchical circuits
  -- box :: 
  -- nbox :: 
  -- box_loopM :: 
  -- loopM_boxed_if :: 

  -- * Block structure
  -- $BLOCK
    
  -- ** Ancillas
  -- $WITHANCILLA
  -- with_ancilla :: Bot
  -- with_ancilla_list :: 
  -- with_ancilla_init :: 
  -- ** Automatic uncomputing
  -- with_computed_fun :: 
  -- with_computed :: 
  -- with_basis_change :: 
  -- ** Controls
  -- with_controls :: ControlSource c => repr vid tf h o c -> repr vid tf i h (Circ a) ->  repr vid tf i h (Circ a) 
  -- with_classical_control :: 
  -- without_controls :: 
  -- without_controls_if :: 
  -- ** Loops
  -- for :: 
  -- endfor :: 
  -- foreach :: 
  -- loop :: 
  -- loop_with_index :: 
  -- loopM :: 
  -- loop_with_indexM :: 
    
  -- * Operations on circuits
  -- ** Reversing
  -- reverse_generic :: 
  -- reverse_simple :: (QCData_Simple x,
  --                    QCData y,
  --                    TupleOrUnary xt x,
  --                    QCurry x_y x y) =>
  --                   UVar repr (x_y -> (y -<> Circ xt))
  reverse_simple :: ( LQCData_Simple repr x, QCData y, LTupleOrUnary repr xt x, LQCurry repr x_y x y ) =>
                    UVar repr (x_y -> y -<> Circ xt)
  -- reverse_generic_endo,
  -- reverse_generic_imp,
  -- reverse_generic_curried,
  -- reverse_simple_curried,
  -- reverse_endo_if,
  -- reverse_imp_if,
  
  -- -- ** Printing
  -- Format (..),
  -- FormatStyle(..),
  -- format_enum,
  -- print_unary,
  -- print_generic,
  -- print_simple fmt circ = Quipper.print_simple fmt $ convert circ
  -- print_of_document,
  -- print_of_document_custom,
  
  -- -- ** Classical circuits  
  -- classical_to_cnot,
  -- classical_to_quantum,
  -- -- ** Ancilla uncomputation
  -- classical_to_reversible,
  
  -- -- * Circuit transformers
  -- -- $TRANSFORMATION
  
  -- -- ** User-definable transformers
  -- Transformer,
  -- T_Gate(..),
  -- -- ** Pre-defined transformers
  -- identity_transformer,
  -- -- ** An example transformer
  -- -- $TRANSEXAMPLE
  
  -- -- ** Applying transformers to circuits
  -- transform_generic,
  -- transform_generic_shape,
  
  -- -- ** Auxiliary type definitions
  -- InverseFlag,
  -- NoControlFlag,
  -- B_Endpoint(..),
  -- Endpoint,
  -- Ctrls,  

  -- -- * Automatic circuit generation from classical code
  -- -- $TEMPLATE
  -- module Quipper.CircLifting,
  -- module Libraries.Template,

  -- -- * Extended quantum data types
  -- -- ** Homogeneous quantum data types
  -- QShape,
  -- QData,
  -- CData,
  -- BData,
  
  -- -- ** Heterogeneous quantum data types
  -- QCData,
  -- QCDataPlus,
  
  -- -- ** Shape-related operations
  -- -- $SHAPE
  bit :: UVar repr (Bit)
  qubit :: UVar repr (Qubit)
  -- qshape,
  -- qc_false,
  
  -- -- ** Quantum type classes
  -- -- $QCLASSES
  -- QEq (..),
  -- QOrd (..),
  -- q_lt,
  -- q_gt,
  -- q_le,
  -- q_ge,




-----------------------------------------------------------------------------------------
  --- for reverse_simple -
  reverse_errmsg :: (QCData x, QCData y) => (String -> String) -> UVar repr ((x -<> Circ y) -> x -> (y -<> Circ x))
  lfmap :: Functor f => UVar repr ((a -<> b) -> f a -<> f b)
-----------------------------------------------------------------------------------------

    
--------------------------------------------------------------------------------
class MLL repr =>
  LQCurry repr fun args res | fun -> args res, args res -> fun where
  lqcurry ::　UVar repr ((args -<> Circ res) -<> fun)
  lquncurry :: UVar repr (fun -<> (args -<> Circ res))
-- instance LQCurry Ev (Cric Qubit) () Qubit => LQCurry Ev (Qubit -<> Circ Qubit) (Qubit * args) Qubit) where
--   lqcurry = llam $ \g -> llam $ \x -> lqcurry ^ (llam $ \xs -> g ^ (x * xs))
--   lquncurry = llam $ \f -> llam $ \xxs -> letStar xxs $ \x xs -> lquncurry ^ (f ^ x) ^ xs
instance MLL repr => LQCurry repr (Circ b) One b where
  lqcurry = llam $ \g -> g ^ one
  lquncurry = llam $ \x -> llam $ \o -> letOne o x
instance (MLL repr, LQCurry repr fun args res) => LQCurry repr (a -<> fun) (a * args) res where
  lqcurry = llam $ \g -> llam $ \x -> lqcurry ^ (llam $ \xs -> g ^ (x * xs))
  lquncurry = llam $ \f -> llam $ \xxs -> letStar xxs $ \x xs -> lquncurry ^ (f ^ x) ^ xs
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
class LinearQuipper repr => LTupleOrUnary repr t s | s -> t where
  -- | For example, maps @(a,(b,(c,(d,()))))@ to @(a,b,c,d)@.
  lweak_tuple :: UVar repr (s -<> t)
  -- | For example, maps @(a,b,c,d)@ to @(a,(b,(c,(d,()))))@.
  lweak_untuple :: UVar repr (t -<> s)

instance LinearQuipper repr => LTupleOrUnary repr One One where
  lweak_tuple = llam $ \o -> letOne o one
  lweak_untuple = llam $ \o -> letOne o one

instance LinearQuipper repr => LTupleOrUnary repr a (a * One) where
  lweak_tuple = llam $ \aOne -> letStar aOne $ \a o -> letOne o a
  lweak_untuple = llam $ \a -> a * one

instance LinearQuipper repr => LTupleOrUnary repr (a * b) (a * (b * One)) where
  lweak_tuple = llam $ \abOne ->
                         letStar abOne $ \a bOne ->
                                           letStar bOne $ \b o -> letOne o (a * b)
  lweak_untuple = llam $ \ab -> letStar ab $ \a b -> (a * (b * one))

instance LinearQuipper repr => LTupleOrUnary repr (a * b * c) (a * (b * (c * One))) where
  lweak_tuple = llam $ \abcOne ->
                         letStar abcOne $ \a bcOne ->
                                            letStar bcOne $ \b cOne ->
                                                              letStar cOne $ \c o -> letOne o (a * b * c)
  lweak_untuple = llam $ \abc ->
                           letStar abc $ \ab c ->
                                           letStar ab $ \a b -> (a * (b * (c * one)))
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- class (LinearQuipper repr,
--        qa ~ QType (CType qa),
--        qa ~ QTypeB (BType qa), 
--        qa ~ QCType Qubit Bool qa,
--        qa ~ QType qa,
--        QCData qa,
--        QCData (CType qa)
--       ) => LQData repr qa
  
-- instance (qa ~ QType (CType qa),
--        qa ~ QTypeB (BType qa), 
--        qa ~ QCType Qubit Bool qa,
--        qa ~ QType qa,       
--        QCData qa,
--        QCData (CType qa)
--       ) => LQData Ev qa
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- class (LinearQuipper repr) => LQCData repr qc where
--   lqcdata_mapM ::
--     (Monad m) => 
--     UVar repr (qc -> (q -> m q') -> (c -> m c') -> QCType q c qc -> m (QCType q' c' qc))
--   lqcdata_zip ::
--     UVar repr (qc -> q -> c -> q' -> c' -> QCType q c qc -> QCType q' c' qc -> ErrMsg ->
--                QCType (q, q') (c, c') qc)
--   lqcdata_promote ::
--     UVar repr (BType qc -> qc -> ErrMsg -> BType qc)

-- instance LQCData Ev Qubit where
--   lqcdata_mapM  = undefined -- f g = f
--   lqcdata_zip = undefined -- q c q' c' x y e = (x, y)
--   lqcdata_promote = undefined -- a x e = a

-- instance LQCData Ev Bit where
--   lqcdata_mapM = undefined -- f g = g
--   lqcdata_zip = undefined -- q c q' c' x y e = (x, y)
--   lqcdata_promote = undefined -- a x e = a

-- instance LQCData Ev One where
--   lqcdata_mapM = undefined -- f g x = return one
--   lqcdata_zip = undefined -- q c q' c' x y e = one
--   lqcdata_promote = undefined -- a x e = a
  
-- instance (LQCData Ev a, LQCData Ev b) => LQCData Ev (a * b) where
--   lqcdata_mapM = undefined -- ~(a,b) f g (x,y) = do
--     -- x' <- qcdata_mapM a f g x
--     -- y' <- qcdata_mapM b f g y
--     -- return (x', y')
--   lqcdata_zip = undefined -- ~(a,b) q c q' c' (x1, x2) (y1, y2) e = (z1, z2) where
--     -- z1 = qcdata_zip a q c q' c' x1 y1 e
--     -- z2 = qcdata_zip b q c q' c' x2 y2 e
--   lqcdata_promote = undefined -- (a,b) (x,y) e = (qcdata_promote a x e, qcdata_promote b y e)

-- instance (LQCData Ev a, LQCData Ev b, LQCData Ev c) => LQCData Ev (a * b * c) where
--   lqcdata_mapM = undefined -- s f g xs = mmap tuple $ qcdata_mapM (untuple s) f g (untuple xs)
--   lqcdata_zip = undefined -- s q c q' c' xs ys e = tuple $ qcdata_zip (untuple s) q c q' c' (untuple xs) (untuple ys) e
--   lqcdata_promote = undefined -- a x s = tuple $ qcdata_promote (untuple a) (untuple x) s
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
class (LinearQuipper repr) => LSimpleType repr qc where
  lfs_shape :: UVar repr qc
  
instance LinearQuipper repr => LSimpleType repr Qubit where
  lfs_shape = LinearQuipper.qubit
  
instance LinearQuipper repr => LSimpleType repr Bit where
  lfs_shape = LinearQuipper.bit

instance LinearQuipper repr => LSimpleType repr One where
  lfs_shape = one
  
instance (LSimpleType repr a, LSimpleType repr b) => LSimpleType repr (a * b) where
  lfs_shape = lfs_shape * lfs_shape

instance (LSimpleType repr a, LSimpleType repr b, LSimpleType repr c) => LSimpleType repr (a * b * c) where
  lfs_shape = lfs_shape * lfs_shape * lfs_shape

instance (LSimpleType repr a, LSimpleType repr b, LSimpleType repr c, LSimpleType repr d) => LSimpleType repr (a * b * c * d) where
  lfs_shape = lfs_shape * lfs_shape * lfs_shape * lfs_shape
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
class (LinearQuipper repr, QCData qc, LSimpleType repr qc) => LQCData_Simple repr qc
instance (LinearQuipper repr, QCData qc, LSimpleType repr qc) => LQCData_Simple repr qc
--------------------------------------------------------------------------------





instance LinearQuipper Ev where

  true = Ev True
  false = Ev False
  --  ascii = Ev $ ASCII


  --   -- * Basic types
  -- Qubit,
  -- Bit,
  -- Qulist,
  -- Bitlist,
  
  -- -- * Basic gates
  -- -- $BASIC
  -- Timestep,
  
  -- -- $FUNCTIONAL_ANCHOR
  
  -- -- ** Reversible gates in functional style
  -- -- $FUNCTIONAL
  qnot = Ev $ Lolli Quipper.qnot
  hadamard = Ev $ Lolli Quipper.hadamard
  gate_H = Ev $ Lolli Quipper.gate_H
  gate_X = Ev $ Lolli Quipper.gate_X
  gate_Y = Ev $ Lolli Quipper.gate_Y
  gate_Z = Ev $ Lolli Quipper.gate_Z
  gate_S = Ev $ Lolli Quipper.gate_S
  gate_S_inv = Ev $ Lolli Quipper.gate_S_inv
  gate_T = Ev $ Lolli Quipper.gate_T
  gate_T_inv = Ev $ Lolli Quipper.gate_T_inv
  gate_E = Ev $ Lolli Quipper.gate_E
  gate_E_inv = Ev $ Lolli Quipper.gate_E_inv
  gate_omega = Ev $ Lolli Quipper.gate_S
  gate_V = Ev $ Lolli Quipper.gate_S
  gate_V_inv = Ev $ Lolli Quipper.gate_V_inv
  -- expZt,
  -- rGate,
  -- gate_W = llam \a -> llam $ \b -> 
  -- gate_iX,
  -- gate_iX_inv,
  -- global_phase,
  -- global_phase_anchored,
  -- qmultinot,
  -- cnot,
  swap = Ev $ Lolli $ \x -> Lolli $ \y -> do
    (q1,q2) <- Quipper.swap x y
    return $ Tensor q1 q2
  
  -- -- $IMPERATIVE_ANCHOR
  
  -- -- ** Reversible gates in imperative style 
  -- -- $IMPERATIVE 
  -- qnot_at,
  -- hadamard_at,
  -- gate_H_at,
  -- gate_X_at,
  -- gate_Y_at,
  -- gate_Z_at,
  -- gate_S_at,
  -- gate_S_inv_at,
  -- gate_T_at,
  -- gate_T_inv_at,
  -- gate_E_at,
  -- gate_E_inv_at,
  -- gate_omega_at,
  -- gate_V_at,
  -- gate_V_inv_at,
  -- expZt_at,
  -- rGate_at,
  -- gate_W_at,
  -- gate_iX_at,
  -- gate_iX_inv_at,
  -- qmultinot_at,
  -- cnot_at,
  -- swap_at,
  
  -- -- ** Gates for state preparation and termination
  qinit = Ev  Quipper.qinit
  -- qterm,
  -- qdiscard,
  -- cinit,
  -- cterm,
  cdiscard = Ev $ \b -> coerce $ Quipper.cdiscard b
  -- qc_init,
  -- qc_init_with_shape,
  -- qc_term,
  -- qc_discard,
  measure = Ev $ Lolli $ \x -> do
    x' <- Quipper.measure x
    return $ toBang x'
  -- prepare,
  -- qc_measure,
  -- qc_prepare,
  
  -- -- ** Gates for classical circuits
  -- -- $CLASSICAL
  -- cgate_xor,  
  -- cgate_eq,
  -- cgate_not,
  -- cgate_and,
  -- cgate_or,
  -- cgate_if,
  -- circ_if,
  
  -- -- ** User-defined gates
  -- named_gate,
  -- named_gate_at,
  -- named_rotation,
  -- named_rotation_at,
  -- extended_named_gate,
  -- extended_named_gate_at,
  
  -- -- ** Dynamic lifting
  -- dynamic_lift,
  
  -- -- * Other circuit-building functions
  -- qinit_plusminus,
  -- qinit_of_char,
  -- qinit_of_string,
  -- map_hadamard,
  -- map_hadamard_at,
  -- controlled_not,
  -- controlled_not_at,
  -- bool_controlled_not,
  -- bool_controlled_not_at,
  -- qc_copy,
  -- qc_uncopy,
  -- qc_copy_fun,
  -- qc_uncopy_fun,
  -- mapUnary,
  -- mapBinary,
  -- mapBinary_c,
  -- qc_mapBinary,
  
  -- -- * Notation for controls
  -- -- $CONTROLS
  -- ControlSource(..),
  -- ControlList,
  -- (.&&.), 
  -- (.==.), 
  -- (./=.),
  controlled q c = Ev $ Quipper.controlled (ev q) (ev c) 
  
  -- -- * Signed items
  -- Signed(..),
  -- from_signed,
  -- get_sign,
  
  -- -- * Comments and labelling
  -- comment,
  -- label,
  -- comment_with_label,
  -- without_comments,
  -- Labelable,
  
  -- -- * Hierarchical circuits
  -- box,
  -- nbox,
  -- box_loopM,
  -- loopM_boxed_if,

  -- -- * Block structure
  -- -- $BLOCK
  
  -- -- ** Ancillas
  -- -- $WITHANCILLA
  -- with_ancilla,
  -- with_ancilla_list,
  -- with_ancilla_init,
  -- -- ** Automatic uncomputing
  -- with_computed_fun,
  -- with_computed,
  -- with_basis_change,
  -- -- ** Controls
  -- with_controls,
  -- with_classical_control,
  -- without_controls,
  -- without_controls_if,
  -- -- ** Loops
  -- for,
  -- endfor,
  -- foreach,
  -- loop,
  -- loop_with_index,
  -- loopM,
  -- loop_with_indexM,
  
  -- -- * Operations on circuits
  -- -- ** Reversing
  -- reverse_generic,
  reverse_simple = ulam $ \f ->
                            (lfmap $$ lweak_tuple) .*. ((reverse_errmsg errmsg $$ (lquncurry ^ f)) $$ lfs_shape)
    where
      errmsg x = "reverse_simple: " ++ x
  -- reverse_generic_endo,
  -- reverse_generic_imp,
  -- reverse_generic_curried,
  -- reverse_simple_curried,
  -- reverse_endo_if,
  -- reverse_imp_if,
  
  -- -- ** Printing
  -- Format (..),
  -- FormatStyle(..),
  -- format_enum,
  -- print_unary,
  -- print_generic,
  -- print_simple fmt circ = Quipper.print_simple fmt $ convert circ
  -- print_of_document,
  -- print_of_document_custom,
  
  -- -- ** Classical circuits  
  -- classical_to_cnot,
  -- classical_to_quantum,
  -- -- ** Ancilla uncomputation
  -- classical_to_reversible,
  
  -- -- * Circuit transformers
  -- -- $TRANSFORMATION
  
  -- -- ** User-definable transformers
  -- Transformer,
  -- T_Gate(..),
  -- -- ** Pre-defined transformers
  -- identity_transformer,
  -- -- ** An example transformer
  -- -- $TRANSEXAMPLE
  
  -- -- ** Applying transformers to circuits
  -- transform_generic,
  -- transform_generic_shape,
  
  -- -- ** Auxiliary type definitions
  -- InverseFlag,
  -- NoControlFlag,
  -- B_Endpoint(..),
  -- Endpoint,
  -- Ctrls,  

  -- -- * Automatic circuit generation from classical code
  -- -- $TEMPLATE
  -- module Quipper.CircLifting,
  -- module Libraries.Template,

  -- -- * Extended quantum data types
  -- -- ** Homogeneous quantum data types
  -- QShape,
  -- QData,
  -- CData,
  -- BData,
  
  -- -- ** Heterogeneous quantum data types
  -- QCData,
  -- QCDataPlus,
  
  -- -- ** Shape-related operations
  -- -- $SHAPE
  bit = Ev Quipper.bit 
  qubit = Ev Quipper.qubit
  -- qshape,
  -- qc_false,
  
  -- -- ** Quantum type classes
  -- -- $QCLASSES
  -- QEq (..),
  -- QOrd (..),
  -- q_lt,
  -- q_gt,
  -- q_le,
  -- q_ge,








  

-----------------------------------------------------------------------------------------
--- for reverse_simple -
  reverse_errmsg e =
    ulam $ \f -> ulam $ \shape -> llam $ \y ->
                                           ((lf_inv $$ f) $$ shape) ^ y
    where
      errmsg x = e ("operation not permitted in reversible circuit: " ++ x) :: String
      f_inv :: (QCData x, QCData y) => (x -> Circ y) -> x -> (y -> Circ x)
      f_inv f shape y = do
        circuit <- Quipper.Generic.encapsulate_generic_in_namespace errmsg f shape
        let circuit_inv = reverse_encapsulated circuit
            f_inv_ = Quipper.Generic.unencapsulate_generic circuit_inv
        f_inv_ y
      lf_inv :: (QCData x, QCData y) => UVar Ev ((x -<> Circ y) -> x -> (y -<> Circ x))
      lf_inv = ulam $ \f -> ulam $ \shape -> Ev $ Lolli $ \yy -> f_inv (unLolli $ ev f) (ev shape) yy
  lfmap = Ev $ \f -> Lolli $ \fa -> fmap (unLolli f) fa
-----------------------------------------------------------------------------------------


 
--  let toP (Tensor a b) = (a,b) in (\a b -> toP <$> ((unLolli $(unLolli $ev qfun) a) b))
-- get_clist :: Circ ControlList
-- get_clist = do
--   s <- get_state
--   return (clist s)
  
-- -- | Set the 'clist' part of the 'Circ' monad's state.
-- set_clist :: ControlList -> Circ ()
-- set_clist clist = do
--   s <- get_state
--   set_state s { clist = clist }

-----------------------------------------------------------------------------------------
type QDefn t = forall repr i vid . (LinearQuipper repr) => repr vid i i t
qdefn :: QDefn t -> QDefn t
qdefn x = x
-----------------------------------------------------------------------------------------
