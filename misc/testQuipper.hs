import Quipper
import QuipperLib.Simulation

plus_minus :: Bool -> Circ Qubit
plus_minus b = do
  q <- qinit b
  r <- hadamard q
  return r


share :: Qubit -> Circ (Qubit, Qubit)
share a = do
  b <- qinit False
  b <- qnot b `controlled` a
  return (a,b)

bell00 :: Circ (Qubit,Qubit)
bell00 = do
  a <- plus_minus False
  (a,b) <- share a
  return (a,b)

alice :: Qubit -> Qubit -> Circ (Bit,Bit)
alice q a = do
  a <- qnot a `controlled` q
  q <- hadamard q
  (x,y) <- measure (q,a)
  return (x,y)

bob :: Qubit -> (Bit,Bit) -> Circ Qubit
bob b (x,y) = do
  b <- gate_X b `controlled` y
  b <- gate_Z b `controlled` x
  cdiscard (x,y)
  return b

teleport :: Qubit -> Circ Qubit 
teleport q = do
  (a,b) <- bell00
  (x,y) <- alice q a
  b <- bob b (x,y)
  return b
   
-----

mycirc1 :: Qubit -> Qubit -> Circ (Qubit, Qubit)
mycirc1 a b = do
  a <- hadamard a
  b <- hadamard b
  (a,b) <- controlled_not a b
  return (a,b) 



  
mycirc2 :: Qubit -> Qubit -> Qubit -> Circ (Qubit, Qubit, Qubit)
mycirc2 a b c = do
  mycirc1 a b
  with_controls c $ do
    mycirc1 a b
    mycirc1 b a
    mycirc1 a c
  return (a,b,c)


mycirc3 :: Qubit -> Qubit -> Qubit -> Circ (Qubit, Qubit, Qubit)
mycirc3 a b c = do
  with_ancilla $ \x -> do
    qnot x `controlled` (a,b)
    hadamard c `controlled` x
    qnot x `controlled` (a,b)
    return (a,b,c)

tcirc1 :: Qubit -> Qubit -> Circ (Qubit, Qubit)
tcirc1 a b = do
  hadamard b
  measure b
  hadamard b
  controlled_not b b
  return (b,b) 

tcirc2 :: Qubit -> Circ Qubit
tcirc2 a = do
  qinit False
  measure a
  hadamard a
  controlled_not a a
  return a 

tcirc3 a = do
  tcirc2 a
  measure a
  

  
-----

badcirc :: Qubit -> Qubit -> Circ Qubit
badcirc q1 q2 = do
  q' <- measure q1
  q'' <- hadamard q1
  return q''

badcirc_ancilla :: Circ Bit
badcirc_ancilla =
  with_ancilla measure


--cloning
badcirc1 :: Qubit -> Qubit -> Circ (Qubit, Qubit)
badcirc1 a b = do
  a <- hadamard a
  b <- hadamard b
  (a,b) <- controlled_not a b
  return (a,a)

--reused
badcirc2 :: Bool -> Circ Qubit
badcirc2 b = do
  q <- qinit b
  r <- hadamard q
  return q

--reused(duplicated...?)
badcirc3 q = do
  q <- qinit True
  (x,y) <- mycirc1 q q
  return (q,x)

badcirc4 :: Qubit -> Qubit -> Circ (Qubit, Qubit)
badcirc4 a b = do
  a <- hadamard a
  b <- hadamard b
  (a,b) <- controlled_not a a
  return (a,b)
-- deletion = do
--   q1 <- qinit False
--   q2 <- qinit True
--   return q2


{-
linerqubit :: v i o a
 -}


-------
circa = swap
circb = swap

circAB :: Qubit -> Qubit -> Qubit
          -> Circ (Qubit, Qubit, Qubit) 
circAB i_1 i_2 i_3 = do 
   (s_1, s_2) <- circa i_1 i_2
   (t_1, t_2) <- circb s_2 i_3 
   return (s_1, t_1, t_2) 


cnot :: Qubit -> Qubit -> Circ (Qubit, Qubit)
cnot a b = do
  a <- qnot a `controlled` b
  return (a, b)


ha :: Qubit -> Qubit ->Circ(Qubit,Qubit, Qubit)
ha a b = do
  (b,c,a) <- with_ancilla ( \c -> do
    c <- qnot c `controlled` (a,b)
    b <- qnot b `controlled` a
    return (b,c,a)
                          )
  c <- qnot c
  
  return (b,c,a)

ccnot :: Qubit -> Qubit -> Circ (Qubit,Qubit,Qubit) 
ccnot a b = do
  c <- qinit False
  c <- qnot c `controlled` (a,b)
  return (c, a, b)

-- nand :: Qubit -> Qubit -> Circ (Qubit,Qubit,Qubit) 
-- nand a b = do
--   c <- qinit False
--   a <- hadamard a
--   b <- hadamard b
--   (c,a,b) <- ccnot c a b
--   c <- qnot c
--   return (c,a,b)

qnothad :: Qubit -> Circ Qubit
qnothad q = do 
  q <- qnot q
  q <- hadamard q
  return q

--qcirc :: Qubit -> Circ (Qubit)
qcirc q = do
  (a, b) <- qinit (True, True)
  a <- hadamard a
  b <- hadamard b
  q' <- qnot q `controlled` (a,b)
  a <- hadamard a
  b <- hadamard b
--  q' <- measure q'
--  qterm (True, True) (a,b)
  return (q') 

  
timestep :: Qubit -> Qubit -> Qubit -> Circ (Qubit, Qubit, Qubit)
timestep a b c = do 
  (a', b') <- mycirc1 a b
  c' <- qnot c `controlled` (a,b)
  (a'', b'') <- reverse_simple mycirc1 (a', b')
  return (a'', b'', c')

example a b = do
  a <- hadamard a
  b <- qnot b
  Main.cnot a b
  
swap' q1 q2= --do
 with_ancilla $ \x -> do
--  x <- qinit False
  cnot x q1 >> cnot x q2
  cnot q1 x >> cnot  q2 x
  cnot x q2 >> cnot x q1
--  qterm False x
  return (q1,q2)
  where
    cnot = Main.cnot

badcirc5 :: Qubit -> Qubit -> Circ Qubit
badcirc5 q1 q2 = do
  q' <- 　qnot q1
  q'' <- hadamard q1
  return q'


test :: Qubit -> Qubit -> Qubit -> Circ (Qubit, Qubit, Qubit)
test a b c = do
  abc <- with_ancilla $ \x -> do
    x <- qnot x `controlled` (a,b)
    (a,b,c) <- mycirc3 a b c `controlled` x
    x <- qnot x `controlled` (a,b)
    return (a,b,c)
  return abc
