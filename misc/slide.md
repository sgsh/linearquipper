

---
# LinearQuipper

---




---
# Linear typing

```haskell
True :: Bool
hadamard :: (Qubit -> Circ Qubit)

true :: !Bool
hadamard' :: !(Qubit ->. Circ Qubit)
```





---
# derive into Haskell by LLC

```haskell
true :: !Bool
hadamard' :: !(Qubit ->. Circ Qubit)

class (LLC repr) => LLCQuipper repr where
   true' ::  UVar repr Bool
   hadamard' :: UVar repr (Qubit -<> Circ Qubit)
```





---
# test






---
# improve Monadic function

```haskell
(>>=) :: Monad m => m a -> (a -> m b) -> m b

(>>=.) :: Monad m => !(m a ->. (a ->. m b) ->. m b) 
```





---
#


```haskell
(>>=.) :: Monad m => !(m a ->. (a ->. m b) ->. m b) 

class (LLC repr) => LLCQuipper repr where
   (>>=.) :: Monad m => UVar repr (m a -<> (UVar repr (a -<> m b)) -<> m b) 

```

# error

```shell
src/llcquipper.hs:194:4-76: error: …
    • Illegal polymorphic type: UVar repr (a -<> m b)
      GHC doesn't yet support impredicative polymorphism
    • When checking the class method:
        (>>=..) :: forall (repr :: Nat
                                   -> Bool
                                   -> [Maybe Nat]
                                   -> [Maybe Nat]
                                   -> ghc-prim-0.5.0.0:GHC.Types.*
                                   -> ghc-prim-0.5.0.0:GHC.Types.*).
                   LLCQuipper repr =>
                   forall (m :: * -> *) a b.
                   Monad m =>
                   UVar repr ((m a -<> UVar repr (a -<> m b)) -<> m b)
      In the class declaration for ‘LLCQuipper’
Compilation failed.
```





---
(>>=.) :: Monad m => !(m a ->. (a ->. m b) ->. m b) 

class LLC (repr :: Nat -> Bool -> [Maybe Nat] -> [Maybe Nat] -> * -> * ) where
   (>>=.) :: ( Or tf1 tf2 tf,
               Monad m ) =>
             repr vid tf1 i h (m a) ->
             repr vid tf2 h o (a -<> m b) ->
             repr vid tf i o (m b)




---

```
:t qinit' true >>=. hadamard' >>=. measure'

:~t qinit' true >>=. clone >>=. measure'
:~t do {q <- qinit' $$ false'; q2 <- measure' ^ q ; return' (q,q2)} 


:t (qinit' $$ false') >>=. measure' >>. return' $$ unit'

:~t (qinit' $$ false') >>. return' $$ top'
```




---
# Cloning and Deletion

```shell
λ> :t do {q <- qinit False ; q2 <- measure q ; return (q,q2)} 
do {q <- qinit False ; q2 <- measure q ; return (q,q2)}
  :: Circ (Qubit, Bit)
  
λ> :t qinit False >>= hadamard >> return ()
qinit False >>= hadamard >> return () :: Circ ()
```
