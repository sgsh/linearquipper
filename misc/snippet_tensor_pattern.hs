-- // LinearQuipper templete
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE ConstraintKinds #-}
-- {-# LANGUAGE AllowAmbiguousTypes #-}
 {-# LANGUAGE TypeFamilyDependencies #-}
-- {-# LANGUAGE ScopedTypeVariables #-}
-- {-# LANGUAGE NoMonoLocalBinds #-}
-- {-# LANGUAGE ExistentialQuantification #-}
-- {-# OPTIONS_GHC -freduction-depth=80 #-}

import Prelude hiding ((*),(+),(^),(>>=),return,(>>))
import LLC
import LLC_M
import Quipper (Circ, Qubit, Bit)
import LinearQuipper
import GHC.Exts (Constraint)

import QuipperLib.Simulation
----------------------------------------------------------------------------
(>>=) ::(LLC_M m repr, Or tf1 tf2 tf, VarOk tf2 var') =>
        repr vid tf1 i h (m a)
      -> (LVar repr vid a -> repr ('S vid) tf2 ('Just vid : h) (var' : o) (m b))
      -> repr vid tf i o (m b)
(>>=) = bind_
return :: LLC_M m repr =>
          repr vid tf i o a -> repr vid tf i o (m a)
return = return_
(>>) :: (VarOk tf2 var', Or tf1 tf2 tf, LLC_M m repr) =>
              repr vid tf1 i h (m One)
              -> repr ('S vid) tf2 ('Nothing : h) (var' : o) (m b)
              -> repr vid tf i o (m b)
(>>) m1 m2 = m1 >>= \x -> letOne x m2 
----------------------------------------------------------------------------
-- LinearQuipper templete //


----------------------------------------------------------------------------------------------
-- 以下のように型族を定義した(Olegさん)
type family Tr (c :: * -> * ) t where
  Tr c (t1 * t2) = (Tr c t1, Tr c t2)
  Tr c t       = c t

-- 変数ジェネレータを扱うには以下のようにすべきでは？しかし　forallがかけない
-- このルール，non-closedにするとconflict
-- TtoPをネストさせないで書くのも，3つ組み以上からconflictなので無理
-- closed-type-famillyにforallとconstraintかけない？
type family (TtoP (r :: Nat -> Bool -> [Maybe Nat] -> [Maybe Nat] -> * -> * ) (vid :: Nat) (b::Bool) (i::[Maybe Nat]) (o::[Maybe Nat]) (t :: *)) :: *
  where
    TtoP r vid b i o (t1 * t2) =
      (TtoP r vid 'False i o t1, TtoP r vid 'False i o t2)
    TtoP r vid b i o t       =
      r vid 'False i o t


-- forall version
-- LVarの展開を考えるとここでConsumeを使いたいが，難しそう．そもそも無理？
type family (TtoPConsume (r :: Nat -> Bool -> [Maybe Nat] -> [Maybe Nat] -> * -> * ) (vid :: Nat) (t :: *)) :: *
-- 3つ組は隠れているけど多分conflict
-- type instance TtoPConsume r vid (t1 * t2 * t3) =
--   forall v1 v2 v3 i1 i2 i3 o1 o2 o3. (Consume vid i1 i2, Consume ('S vid) i2 o2, Consume ('S ('S vid)) i3 o3) =>
--   (r v1 'False i1 o1 t1, r v2 'False i2 o2 t2, r v3 'False i3 o3 t3)
-- type instance TtoPConsume r vid (t1 * t2) =
--   forall v1 v2 i1 i2 o1 o2. (Consume vid i1 i2, Consume ('S vid) i2 o2) =>
--   (r v1 'False i1 o1 t1, r v2 'False i2 o2 t2)
-- type instance TtoPConsume r vid t       =
--   forall (v::Nat) (i'::[Maybe Nat]) (o'::[Maybe Nat]). Consume vid i' o' =>
--   r v 'False i' o' t

----------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
-- 下のようなのは無理？
type family Curr (c :: * -> * ) a
  where
  Curr c (t1 * t2) = Curr c t1 -> Curr c t2
  Curr c t1       = c t1

-- forallがかけるならさらに以下のようにしたい
type family Curr' (r :: Nat -> Bool -> [Maybe Nat] -> [Maybe Nat] -> * -> *) (vid::Nat) a
  -- where
  -- Curr' r vid (t1 * t2) = Curr' r vid t1 -> Curr' r ('S vid) t2
  -- Curr' r vid t       = 
  --   forall (v :: Nat) (i :: [Maybe Nat]) (o :: [Maybe Nat]).
  --   Consume vid i o => 
  --      r v 'False i o t

-- これもillegal-poly
type family (LVars (repr :: Nat -> Bool -> [Maybe Nat] -> [Maybe Nat] -> * -> *) (vid::Nat) (t :: *)) :: * where
  LVars repr vid (t1 * t2) = LVars repr vid t1 -> LVars repr ('S vid) t2
  -- LVars repr vid t = LVar repr vid t
----------------------------------------------------------------------------------------------

{-
class LLC repr => TensP (repr :: Nat -> Bool -> [Maybe Nat] -> [Maybe Nat] -> * -> * ) (xy :: *) where
  type (TtoP' (r :: Nat -> Bool -> [Maybe Nat] -> [Maybe Nat] -> * -> * ) (vid :: Nat) (xy :: *)) :: *
  letSS ::
    --  ( Or b2 b1 b, Consume vid h o,  VarOk b2 var ) =>
    ( Or b2 b1 b, Consume vid i h, Consume ('S vid) i h, VarOk b2 var1, VarOk b2 var2 ) =>
    repr vid b1 i h xy ->
    (TtoP repr vid 'False i h xy ->
    -- (TtoP repr vid 'False h o xy ->
    --  repr  ('S vid) b2 ('Just vid ':i) (var ': h) z) ->
--      repr  ('S('S vid)) b2 ('Just vid ': 'Just ('S vid) ':h) (var1 ': var2 ': o) z) ->
      repr  vid2 b2 ('Just vid ': 'Just ('S vid) ':h) (var1 ': var2 ': o) z) ->
    repr vid2 b i o z
-}

{-
instance ( LLC repr ) => TensP repr Qubit where
  -- assosciated-type-famillyにしてもillegal poly
   -- type TtoP' repr vid Qubit       =
   --  forall (v::Nat) (i'::[Maybe Nat]) (o'::[Maybe Nat]). Consume vid i' o' => repr v 'False i' o' Qubit
  -- 下は一時的にコメントアウト
  -- pvarを参考にすると型つくけど自分が正しいと思っている型だとエラー，何か誤解？
  letSS ::
    ( Or b2 b1 b, VarOk b2 var, Consume vid i h ) =>
    repr vid b1 i h Qubit ->
    (repr vid 'False h o Qubit ->
     repr  ('S vid) b2 ('Just vid : h) (var ': o) z) ->
    repr vid b i o z
  letSS x f = llam f ^ x
instance (TensP repr t1, TensP repr t2) => TensP repr (t1 * t2) where
  -- assosciated-type-famillyにしてもilligal poly
  -- type TtoP' repr vid (t1 * t2) =
  --   forall v1 v2 i1 i2 o1 o2. (Consume vid i1 i2, Consume ('S vid) i2 o2) => (repr v1 'False i1 o1 t1, repr v2 'False i2 o2 t2)
  letSS ::     
    ( Or b2 b1 b, Consume vid i h, Consume ('S vid) i h, VarOk b2 var1, VarOk b2 var2 ) =>
    repr vid b1 i h (x * y) ->
--    ((TtoP repr vid 'False h o (x * y)) ->
    ((TtoP repr vid 'False h o x, TtoP repr ('S vid) 'False h o y) ->
     repr ('S('S vid)) b2 ('Just vid ': 'Just ('S vid) ': h) (var1 ': var2 ': o) z) ->
    repr vid b i o z
  letSS xy f =
    
    letStar xy $ \x y ->   -- f (x, y) --これもネストしなくてもエラーだ
                   -- letSS x (\x ->
                   --            letSS y (\y -> f (x, y)))
////////
-}
-- Quipper.print_simple Quipper.Preview $ convert challenge_letSS1
{-
challenge_letSS1 ::
  LinearQuipper repr =>
  UVar repr (Qubit -<> Circ (Qubit))
challenge_letSS1 = llam $ \q -> 
  letSS q $ \q -> hadamard ^ q
-}


challenge_letSS2_swap ::
  LinearQuipper repr =>
  UVar repr (Qubit -<> Qubit -<> Circ (Qubit * Qubit))
challenge_letSS2_swap = qdefn $ llam' $ \q1 -> llam' $ \q2 ->
  letSS (q1 * q2) $ \(LV q1, LV q2) -> swap ^ q1 ^ q2

test_swap :: LinearQuipper repr => UVar repr (Qubit -<> Qubit -<> Circ(Qubit * Qubit))
test_swap = qdefn $ llam' $ \q1 -> llam' $ \q2 ->
  letStar (q1 * q2) $ \q1 q2 -> swap ^ q1 ^ q2

challenge_letSS2_mes ::
  LinearQuipper repr =>
  UVar repr (Qubit -<> Qubit -<> Circ (Bang Bit * Bang Bit))
challenge_letSS2_mes = qdefn $ llam' $ \q1 -> llam' $ \q2 ->
  letSS (q1 * q2) $ \(LV q1, LV q2) -> measure ^ (q1 * q2)

-- test_mes :: LinearQuipper repr => UVar repr (Qubit -<> Qubit -<> Circ(Bang Bit * Bang Bit))
-- test_mes = qdefn $ llam' $ \q1 -> llam' $ \q2 ->
--   letStar (q1 * q2) $ \q1 q2 -> measure ^ (q1 * q2)

challenge_letSS3 ::
  LinearQuipper repr =>
  UVar repr (Qubit -<>  Qubit -<> Qubit -<> Circ (Bang Bit * Bang Bit * Bang Bit))
challenge_letSS3 = llam' $ \q1 -> llam' $ \q2 -> llam' $ \q3 -> 
  letSS (q1 * q2 * q3) $ \((LV q1, LV q2), LV q3) -> measure ^ (q1 * q2 * q3)

challenge_letSS4 ::
  LinearQuipper repr =>
  UVar repr (Qubit -<> Qubit -<>  Qubit -<> Qubit -<> Circ (Bang Bit * Bang Bit * Bang Bit * Bang Bit))
challenge_letSS4 = llam' $ \q1 -> llam' $ \q2 -> llam' $ \q3 -> llam' $ \q4 -> 
  letSS (q1 * q2 * q3 * q4) $ \(((LV q1, LV q2), LV q3), LV q4) -> measure ^ (q1 * q2 * q3 * q4)

challenge_letSS4' ::
  LinearQuipper repr =>
  UVar repr ((Qubit * Qubit) -<>  Qubit -<> Qubit -<> Circ (Bang Bit * Bang Bit * Bang Bit * Bang Bit))
challenge_letSS4' = llam' $ \q12  -> llam' $ \q3 -> llam' $ \q4 -> 
  letSS (q12 * q3 * q4) $ \(((LV q1, LV q2), LV q3), LV q4) -> measure ^ (q1 * q2 * q3 * q4)

challenge_letSS_complex ::
  LinearQuipper repr =>
  UVar repr ((Qubit * Qubit) -<>  Qubit -<> Qubit -<> Circ ((Bang Bit * Bang Bit) * Qubit * Qubit))
challenge_letSS_complex = llam' $ \q12  -> llam' $ \q3 -> llam' $ \q4 -> 
  letSS (q3 * q4) $ \((LV q3), LV q4) -> do
  q12 <- measure ^ q12
  q3 <- hadamard ^ q3
  q34 <- swap ^ q3 ^ q4
  letSS q34 $ \(LV q3,LV q4) -> return (q12*q3*q4)
--  measure ^ (q1 * q2 * q3 * q4)


challenge_letSS2'' :: LinearQuipper repr => UVar repr (((Bang Bit) * (Bang Bit)) -<> Circ Bit)
challenge_letSS2'' = llam' $ \ab ->
  letSS ab $ \ (UV a,UV b) -> do
  return a

challenge_letSS3'' :: LinearQuipper repr => UVar repr (((Bang Bit) * (Bang Bit) * (Bang Bit)) -<> Circ Bit)
challenge_letSS3'' = llam' $ \abc ->
  letSS abc $ \ ((UV a,UV b), UV c) -> do
  return a

check :: LinearQuipper repr => UVar repr (Qubit -<> Qubit -<> Qubit -<> Circ Qubit)
check =  qdefn $ llam' $ \q -> llam' $ \a -> llam' $ \b -> do
    xy <- measure ^ (q * a)
    letSS xy $ \(UV x, UV y) -> do
      bob ^ b $$ x $$ y

challenge_letSS_complex3 ::
  LinearQuipper repr =>
  UVar repr ((Qubit * Qubit) -<>  (Qubit * Qubit) -<> Circ (Qubit * Qubit))
challenge_letSS_complex3 = llam' $ \q12  -> llam' $ \q34 -> do
  q12' <- measure ^ q12
  letSS q12' $ \(UV a, UV b) -> 
    letSS q34 $  \(LV q3, LV q4) -> do
    q3' <- hadamard ^ q3 `controlled` a
    q34' <- swap ^ q3' ^ q4
    return (q34')

{-
challenge_letSS3_swp ::
  LinearQuipper repr =>
  UVar repr (Qubit -<>  Qubit -<> Qubit -<> Circ (Qubit * Qubit * Qubit))
-}
-- challenge_letSS3_swp = llam' $ \q1 -> llam' $ \q2 -> llam' $ \q3 -> 
--   letSS (q1 * q2 * q3) $ \((LV q1, LV q2), LV q3) -> do
--   ab <- swap ^ q1 ^ q2
--   return (ab*q3)

-- challenge_letSS3_swp' = llam' $ \q1 -> llam' $ \q2 -> llam' $ \q3 -> 
--   l2 (q1 * q2 * q3) $ \((LV q1, LV q2), LV q3) -> do
--   ab <- swap ^ q1 ^ q2
--   return (ab*q3)

{-
letSS' ::
  ( LLC repr ,Or b2 b1 b, VarOk b2 var, Consume vid k l ) =>
  repr vid b1 h o Qubit ->
  (repr v 'False k l Qubit ->
--    repr  ('S vid) b2 ('Just vid : i) (var ': h) z) -> 
    repr  vid1 b2 i1 h1 z) -> 
  repr vid1 b i1 o z
letSS' x f =  f  x
-}
{-
letStar1 :: (LLC repr, VarOk tf1 'Nothing , Or tf0 tf1 tf )
    => repr vid tf0 i h (a * b)
    -> (LVar repr vid a -> LVar repr ('S vid) b -> repr ('S ('S vid)) tf1 ('Just vid ': 'Just ('S vid) ': h) ('Nothing ': 'Nothing ': o) c )
    -> repr vid tf i o c

letStar1 ab f =
    let Ev (a, b) = ab in
      Ev $ (\a b -> ev $ (f) (Ev a) (Ev b)) a b
-}
letSS2
  :: (LLC repr,Or tf0 tf1 tf,
      Consume vid i1 o1, VarOk tf1 'Nothing, Consume ('S vid) i2 o2) =>
     repr vid tf0 i3 h (a * b)
     -> ((repr v1 'False i1 o1 a, repr v2 'False i2 o2 b)
         -> repr ('S ('S vid)) tf1 ('Just vid : 'Just ('S vid) : h) ('Nothing
                                                                       : 'Nothing : o3) c)
     -> repr vid tf i3 o3 c

letSS2 xy f =  letStar xy $ \x y -> f (x,y)


letSS3 :: (Or tf0 tf1 tf, LLC repr, VarOk tf1 'Nothing, Consume ('S vid) i1 o1,
                 Consume ('S ('S vid)) i2 o2, Consume ('S ('S ('S vid))) i3 o3) =>
                repr vid tf0 i4 h ((a * b1) * b2)
                -> (((repr v1 'False i2 o2 a, repr v2 'False i3 o3 b1),
                     repr v3 'False i1 o1 b2)
                    -> repr ('S ('S ('S ('S vid)))) tf1 ('Just ('S ('S vid))
                                                           : 'Just ('S ('S ('S vid))) : 'Nothing
                                                           : 'Just ('S vid) : h) ('Nothing
                                                                                    : 'Nothing : 'Nothing
                                                                                    : 'Nothing : o4) c)
                -> repr vid tf i4 o4 c

letSS3 xyz f = 
    letStar xyz $ \xy z -> letSS2 xy $ \xy -> f (xy,z)

{-
letSS4 :: (Or tf0 tf1 tf, LLC repr, VarOk tf1 var1, VarOk tf1 var0,
                 VarOk tf1 var2, VarOk tf1 var3, VarOk tf1 var4, VarOk tf1 var5,
                 Consume ('S vid) i1 o1, Consume ('S ('S ('S vid))) i2 o2,
                 Consume ('S ('S ('S ('S vid)))) i3 o3,
                 Consume ('S ('S ('S ('S ('S vid))))) i4 o4) =>
                repr vid tf0 i5 h (((a * b1) * b2) * b)
                -> ((((repr v1 'False i3 o3 a, repr v2 'False i4 o4 b1),
                      repr v3 'False i2 o2 b2),
                     repr v 'False i1 o1 b)
                    -> repr ('S ('S ('S ('S ('S ('S vid)))))) tf1 ('Just
                                                                     ('S ('S ('S ('S vid))))
                                                                     : 'Just
                                                                         ('S
                                                                            ('S ('S ('S ('S vid)))))
                                                                     : 'Nothing
                                                                     : 'Just ('S ('S ('S vid)))
                                                                     : 'Nothing : 'Just ('S vid)
                                                                     : h) (var4
                                                                             : var5 : var3 : var2
                                                                             : var0 : var1 : o5) c)
                -> repr vid tf i5 o5 c

letSS4 xyzu f = 
    letStar xyzu $ \xyz u -> letSS3 xyz $ \xyz'-> f (xyz',u)

-}



l1 :: (LLC repr, Or tf1 tf2 tf, VarOk tf1 'Nothing) =>
                         repr vid tf2 h o a
                         -> (LV repr vid a
                             -> repr ('S vid) tf1 ('Just vid : i) ('Nothing : h) b)
                         -> repr vid tf i o b

l1 ab f = llam (\x -> f (LV x))  ^ ab

l2 :: (Or tf0 tf1 tf, LLC repr) =>
  repr vid tf0 i h (a * a1)
  -> ((LV repr vid a, LV repr ('S vid) a1) -> repr ('S ('S vid)) tf1 ('Just vid
                                                          : 'Just ('S vid) : h) ('Nothing: 'Nothing: o) c)
                         -> repr vid tf i o c

l2 ab f = letStar' ab $ \ x y -> f (LV x, LV y)

l3 :: (LLC repr, Or tf0 tf1 tf) =>
                         repr vid tf0 i h ((a * a1) * a2)
                         -> (((LV repr ('S ('S vid)) a, LV repr ('S ('S ('S vid))) a1),
                              LV repr ('S vid) a2)
                             -> repr ('S ('S ('S ('S vid)))) tf1 ('Just ('S ('S vid))
                                                                    : 'Just ('S ('S ('S vid)))
                                                                    : 'Nothing : 'Just ('S vid)
                                                                    : h) ('Nothing
                                                                            : 'Nothing : 'Nothing
                                                                            : 'Nothing : o) c)
                         -> repr vid tf i o c
l3 abc f = letStar' abc $ \ ab c ->
  l2 ab $ \ ab -> f (ab,LV c)

l4 :: (LLC repr, Or tf0 tf1 tf) =>
                         repr vid tf0 i h (((a * a1) * a2) * a4)
                         -> ((((LV repr ('S ('S ('S ('S vid)))) a,
                                LV repr ('S ('S ('S ('S ('S vid))))) a1),
                               LV repr ('S ('S ('S vid))) a2),
                              LV repr ('S vid) a4)
                             -> repr ('S ('S ('S ('S ('S ('S vid)))))) tf1 ('Just
                                                                              ('S
                                                                                 ('S ('S ('S vid))))
                                                                              : 'Just
                                                                                  ('S
                                                                                     ('S
                                                                                        ('S
                                                                                           ('S
                                                                                              ('S
                                                                                                 vid)))))
                                                                              : 'Nothing
                                                                              : 'Just
                                                                                  ('S ('S ('S vid)))
                                                                              : 'Nothing
                                                                              : 'Just ('S vid)
                                                                              : h) ('Nothing
                                                                                      : 'Nothing
                                                                                      : 'Nothing
                                                                                      : 'Nothing
                                                                                      : 'Nothing
                                                                                      : 'Nothing
                                                                                      : o) c)
                         -> repr vid tf i o c
                         
l4 abcd f = letStar' abcd $ \abc d ->
  l3 abc $ \ abc -> f (abc,LV d)

ll3 ::  (LLC repr, Or tf0 tf1 tf) =>
             repr vid tf0 i h (a1 * (a2 * a3))
             -> ((LV repr vid a1,
                  (LV repr ('S ('S vid)) a2, LV repr ('S ('S ('S vid))) a3))
                 -> repr ('S ('S ('S ('S vid)))) tf1 ('Just ('S ('S vid))
                                                         : 'Just ('S ('S ('S vid))) : 'Just vid
                                                         : 'Nothing : h) ('Nothing
                                                                            : 'Nothing : 'Nothing
                                                                            : 'Nothing : o) c)
             -> repr vid tf i o c
ll3 abc f = letStar' abc $ \a bc -> l2 bc $ \bc -> f (LV a, bc) 

ll4 :: LLC repr =>
             repr vid tf0 i h (a1 * ((a2 * a3) * a4))
             -> ((LV repr vid a1,
                  ((LV repr ('S ('S ('S ('S vid)))) a2,
                    LV repr ('S ('S ('S ('S ('S vid))))) a3),
                   LV repr ('S ('S ('S vid))) a4))
                 -> repr ('S ('S ('S ('S ('S ('S vid)))))) tf1 ('Just
                                                                   ('S ('S ('S ('S vid))))
                                                                   : 'Just
                                                                       ('S ('S ('S ('S ('S vid)))))
                                                                   : 'Nothing
                                                                   : 'Just ('S ('S ('S vid)))
                                                                   : 'Just vid : 'Nothing
                                                                   : h) ('Nothing
                                                                           : 'Nothing : 'Nothing
                                                                           : 'Nothing : 'Nothing
                                                                           : 'Nothing : o) c)
             -> repr vid tf i o c
ll4 abcd f = letStar' abcd $ \a bcd -> l3 bcd $ \bcd -> f (LV a, bcd)

l2l2 :: LLC repr =>
              repr vid tf0 i h ((a2 * a3) * (a4 * a5))
              -> (((LV repr ('S ('S vid)) a2, LV repr ('S ('S ('S vid))) a3),
                   (LV repr ('S ('S ('S ('S vid)))) a4, LV repr ('S ('S ('S ('S ('S vid))))) a5))
                  -> repr ('S ('S ('S ('S ('S ('S vid)))))) tf1 ('Just
                                                                   ('S ('S ('S ('S vid))))
                                                                   : 'Just
                                                                       ('S ('S ('S ('S ('S vid)))))
                                                                   : 'Just ('S ('S vid))
                                                                   : 'Just ('S ('S ('S vid)))
                                                                   : 'Nothing : 'Nothing
                                                                   : h)
                   ('Nothing
                                                                           : 'Nothing : 'Nothing
                                                                           : 'Nothing : 'Nothing
                                                                           : 'Nothing : o) c)
              -> repr vid tf i o c
l2l2 abcd f = letStar' abcd$ \ab cd -> l2 ab $ \ab -> l2 cd $ \cd -> f  (ab, cd)

l2l3 :: LLC repr =>
              repr vid tf0 i h ((a2 * a3) * ((a4 * a5) * a6))
              -> (((LV repr ('S ('S vid)) a2, LV repr ('S ('S ('S vid))) a3),
                   ((LV repr ('S ('S ('S ('S ('S ('S vid)))))) a4,
                     LV repr ('S ('S ('S ('S ('S ('S ('S vid))))))) a5),
                    LV repr ('S ('S ('S ('S ('S vid))))) a6))
                  -> repr ('S ('S ('S ('S ('S ('S ('S ('S vid)))))))) tf1 ('Just
                                                                             ('S
                                                                                ('S
                                                                                   ('S
                                                                                      ('S
                                                                                         ('S
                                                                                            ('S
                                                                                               vid))))))
                                                                             : 'Just
                                                                                 ('S
                                                                                    ('S
                                                                                       ('S
                                                                                          ('S
                                                                                             ('S
                                                                                                ('S
                                                                                                   ('S
                                                                                                      vid)))))))
                                                                             : 'Nothing
                                                                             : 'Just
                                                                                 ('S
                                                                                    ('S
                                                                                       ('S
                                                                                          ('S
                                                                                             ('S
                                                                                                vid)))))
                                                                             : 'Just ('S ('S vid))
                                                                             : 'Just
                                                                                 ('S ('S ('S vid)))
                                                                             : 'Nothing : 'Nothing
                                                                             : h) ('Nothing
                                                                                     : 'Nothing
                                                                                     : 'Nothing
                                                                                     : 'Nothing
                                                                                     : 'Nothing
                                                                                     : 'Nothing
                                                                                     : 'Nothing
                                                                                     : 'Nothing
                                                                                     : o) c)
              -> repr vid tf i o c
l2l3 abcde f = letStar' abcde $ \ab cde -> l2 ab $ \ab -> l3 cde $ \cde -> f  (ab, cde)

b2 :: (Or tf0 tf1 tf, LLC repr) =>
            repr vid tf0 i h (Bang (Bang a))
            -> (UV repr a -> repr vid tf1 h o b) -> repr vid tf i o b
b2 bba f = letBang bba $ \ba -> letBang ba $ \a -> f (UV a)

b3 :: (Or tf0 tf1 tf, LLC repr) =>
            repr vid tf0 i h (Bang (Bang (Bang a)))
            -> (UV repr a -> repr vid tf1 h o b) -> repr vid tf i o b
b3 bbba f = letBang bbba $ \bba -> b2 bba $ \a -> f a

-- u1 a f = ulam (\x -> f (UV x))  $$ a

-- u2 ab f = letStar' ab $ \ x y -> letBang x $ \x -> letBang y $ \y -> f (UV x, UV y)

-- u3 abc f = letStar' abc $ \ ab c -> u2  ab $ \ab -> letBang c $ \c -> f (ab,UV c)


newtype UV (repr :: Nat
                   -> Bool -> [Maybe Nat] -> [Maybe Nat] -> * -> *)
          a = 
  UV {unUV :: UVar repr a}
  
newtype LV (repr :: Nat
                   -> Bool -> [Maybe Nat] -> [Maybe Nat] -> * -> *)
          (vid :: Nat)
          a = 
  LV {unLV ::LVar repr vid a}

class LLC repr =>
  TensP (repr :: Nat -> Bool -> [Maybe Nat] -> [Maybe Nat] -> * -> * )
  ab abpair vid vido h hout o oout  |
  ab repr vid -> abpair,  vid abpair -> vido, vido -> vid, hout -> h, vid h abpair -> hout, oout -> o
 where
  letSS ::(Or tf0 tf1 tf) =>
     repr vid tf0 i h ab ->
     (abpair -> repr vido tf1 hout oout c) ->
     repr vid tf i o c

instance LLC repr =>
         TensP repr
         (Qubit * Qubit)
         (LV repr vid Qubit, LV repr ('S vid) Qubit)
         vid
         ('S ('S vid))
         h
         ('Just vid : 'Just ('S vid) : h)
         o
         ('Nothing ': 'Nothing : o)
         where
           letSS :: (Or tf0 tf1 tf) =>
             repr vid tf0 i h (Qubit * Qubit)
             -> ((LV repr vid Qubit, LV repr ('S vid) Qubit) -> repr ('S ('S vid)) tf1 ('Just vid : 'Just ('S vid) : h) ('Nothing: 'Nothing: o) c)
                         -> repr vid tf i o c
           letSS ab f = letStar' ab $ \ x y -> f (LV x, LV y)
           


instance (LLC repr)=>
         TensP repr
         (Qubit * (Bang t))
         (LV repr vid Qubit, UV repr  t)
         vid
         ('S ('S vid))
         h
         ('Just vid : 'Nothing : h)
         o
         ('Nothing ': 'Nothing : o)
         where
           letSS :: --(Or tf0 tf1 tf) =>
             repr vid tf0 i h (Qubit * (Bang t)) ->
             ((LV repr vid Qubit, UV repr t) -> repr ('S ('S vid)) tf1 ('Just vid : 'Nothing : h) ('Nothing : 'Nothing : o) c) ->
             repr vid tf i o c
           letSS ab f = letStar' ab $ \ x y -> letBang y $ \y ->  f (LV x, UV y)


instance (LLC repr)=>
         TensP repr
         ((Bang t) * Qubit)
         ( UV repr t, LV repr ('S vid) Qubit)
         vid
         ('S ('S vid))
         h
         ('Nothing : 'Just ('S vid) : h)
         o
         ('Nothing ': 'Nothing : o)
         where
           letSS :: -- (Or tf0 tf1 tf) =>
             repr vid tf0 i h (Bang t1 * Qubit)  ->
             ((UV repr t1, LV repr ('S vid) Qubit) -> repr ('S ('S vid)) tf1 ('Nothing : 'Just ('S vid) : h) ('Nothing : 'Nothing : o) c) ->
             repr vid tf i o c
           letSS xy f = letStar' xy $ \ x y -> letBang x $ \ x ->  f (UV x, LV y)

instance (LLC repr)=>
         TensP repr
         ((Bang t1) * (Bang t2))
         (UV repr t1, UV repr  t2)
         vid
         ('S ('S vid))
         h
         ('Nothing ': 'Nothing : h)
         o
         ('Nothing ': 'Nothing : o)
         where
           letSS ::
             repr vid tf0 i h (Bang t1 * Bang t2) ->
             ((UV repr t1, UV repr  t2) ->
              repr ('S ('S vid)) tf1 ('Nothing : 'Nothing : h) ('Nothing : 'Nothing : o) c) ->
             repr vid tf i o c
           letSS ab f = letStar' ab $ \ x y -> letBang x $ \x -> letBang y $ \y ->  f (UV x, UV y)

instance ( LLC repr, TensP repr (a*b)  abpair ('S('S vid)) vido ('Nothing : 'Nothing : h) hout ('Nothing : 'Nothing : o) oout ) =>
           TensP repr
           ((a*b)* (Bang a2))
           (abpair, UV repr a2)
           vid
           vido
           h
           hout
           o
           oout
           where
             letSS :: -- (Or tf0 tf1 tf) =>
                     repr vid tf0 i h ((a*b) * Bang a2) ->
                     ((abpair, UV repr a2) -> repr vido tf1 hout oout c) ->
                     repr vid tf i o c
             letSS abc f = letStar' abc $ \ ab c -> letBang c $ \c -> letSS ab $ \ ab -> f (ab,UV c)
           
instance (LLC repr, TensP repr (t1 * t2) abpair ('S('S vid)) vido ('Nothing : 'Just ('S vid) : h) hout ('Nothing ': 'Nothing : o) oout) =>
         TensP repr
         ((t1 * t2) * Qubit)
         (abpair, LV repr ('S vid) Qubit)
         vid
         vido
         h
         hout
         o
         oout
         where
           letSS :: (Or tf0 tf1 tf) =>
                         repr vid tf0 i h ((t1*t2) * Qubit)
                         -> ((abpair, LV repr ('S vid) Qubit) -> repr vido tf1 hout oout c)
                         -> repr vid tf i o c
           letSS abc f = letStar' abc $ \ ab c -> letSS ab $ \ ab -> f (ab,LV c)


instance (LLC repr, TensP repr (t1 * t2) abpair ('S('S vid)) vido ('Just vid : 'Nothing : h) hout ('Nothing ': 'Nothing : o) oout) =>
         TensP repr
         (Qubit * (t1 * t2))
         (LV repr (vid) Qubit, abpair)
         vid
         vido
         h
         hout
         o
         oout
         where
           letSS abc f = letStar' abc $ \ a bc -> letSS bc $ \ bc -> f (LV a, bc)

instance (LLC repr, TensP repr (t1 * t2) abpair ('S('S vid)) vido ('Nothing: 'Nothing : h) hout ('Nothing ': 'Nothing : o) oout) =>
         TensP repr
         ((Bang t) * (t1 * t2))
         (UV repr t, abpair)
         vid
         vido
         h
         hout
         o
         oout
         where
           letSS abc f =  letStar' abc $ \ a bc -> letBang a $ \a -> letSS bc $ \ bc -> f (UV a, bc)

-- f1 :: (TensP repr ab1 b vido1 vido2 h1 hout1 oout1 oout2,
--              TensP
--                repr
--                ab2
--                a
--                ('S ('S vid))
--                vido1
--                ('Nothing : 'Just ('S vid) : h2)
--                hout2
--                ('Nothing : 'Nothing : o)
--                oout1,
--              Consume ('S vid) hout2 h1) =>
--             repr vid tf0 i h2 (ab2 * ab1)
--             -> ((a, b) -> repr vido2 tf1 hout1 oout2 c) -> repr vid tf i o c
-- f1 abcd f = letStar' abcd $ \ ab cd -> letSS ab $ \ ab -> letSS cd $ \cd -> f (ab, cd)


instance (LLC repr
         ,TensP repr (t1*t2) abpair ('S ('S vid)) vido' ('Nothing : 'Just ('S vid) : h) hout' ('Nothing : 'Nothing : o)  oout'
         ,TensP repr (t3*t4) cdpair vido' vido hout' hout oout' oout
         ,Consume ('S vid) hout' hout'
         ) =>
     -- repr vid tf0 i h2 (ab1 * ab2)
     -- -> ((a, b) -> repr vido2 tf1 hout1 oout2 c) -> repr vid tf i o c
         TensP repr
         ((t1 * t2) * (t3 * t4))
         (abpair,cdpair)
         vid
         vido
         h
         hout
         o
         oout
         where
           letSS :: (Or tf0 tf1 tf) =>
                         repr vid tf0 i h ((t1*t2) * (t3*t4))
                         -> ((abpair, cdpair) -> repr vido tf1 hout oout c)
                         -> repr vid tf i o c
           letSS abcd f = letStar' abcd $ \ ab cd -> letSS ab $ \ ab -> letSS cd $ \cd -> f (ab, cd)

-- -- !(! a) conflict...
-- instance (LLC repr) =>
--          TensP repr
--          (Bang Bit)
--          (UV repr Bit)
--          vid
--          vid
--          h
--          h
--          o
--          o
--          where
--            letSS ba f =  letBang ba $ \a -> f (UV a)

-- instance (LLC repr) =>
--          TensP repr
--          (Bang (Bang t))
--          (UV repr (Bang t))
--          vid
--          vid
--          h
--          h
--          o
--          o
--          where
--            letSS bbna f =  letBang bbna $ \bna -> letSS bna $ \a -> f a
           
{-
instance (LLC repr, TensP repr ((a * b) *c)  abpair ('S ('S vid)) vido ('Nothing : 'Just ('S vid) : h) hout ('Nothing ': 'Nothing : o) oout) =>
         TensP repr
         (((a * b) * c) * Qubit)
         (abpair, LV repr ('S vid) Qubit)
         vid
         vido
         h
         hout
         o
         oout
         where
           letSS :: (Or tf0 tf1 tf) =>
                         repr vid tf0 i h (((a*b) * c) * Qubit)
                         -> ((abpair,LV repr ('S vid) Qubit)
                             -> repr vido tf1 hout oout w)
                         -> repr vid tf i o w
           letSS abcd f = letStar' abcd $ \ abc d -> letSS abc $ \ abc -> f (abc,LV d)

-}


{-
class LLC repr =>
  TensP (repr :: Nat -> Bool -> [Maybe Nat] -> [Maybe Nat] -> * -> * ) ab (vio :: k) abpair vid vido h hout o3 o3out (consumeC :: Constraint) |
  ab vio -> abpair, abpair -> ab vio, vido -> vid,  hout -> h vid, ab hout vio -> consumeC, o3out -> o3
  -- ab vio -> abpair, abpair -> ab vio, vido -> vid, hout -> h vid, vio vid vido -> consumeC, o3out -> o3, h vid -> hout 
 where
  letSS ::(Or tf0 tf1 tf, VarOk tf1 'Nothing, consumeC) =>
     repr vid tf0 i3 h ab ->
     (abpair -> repr vido tf1 hout o3out c) ->
     repr vid tf i3 o3 c

instance LLC repr =>
         TensP repr
         (Qubit * Qubit)
         [repr v1 'False i1 o1, repr v2 'False i2 o2]
         (repr v1 'False i1 o1 Qubit, repr v2 'False i2 o2 Qubit)
         vid
         ('S ('S vid))
         h
         ('Just vid : 'Just ('S vid) : h)
         o3
         ('Nothing ': 'Nothing : o3)
         (Consume vid i1 o1, Consume ('S vid) i2 o2)
  where
    letSS
      :: (Or tf0 tf1 tf, VarOk tf1 'Nothing,
           Consume vid i1 o1, Consume ('S vid) i2 o2) =>
         repr vid tf0 i3 h (Qubit * Qubit) ->
         ((repr v1 'False i1 o1 Qubit, repr v2 'False i2 o2 Qubit) ->
          repr ('S ('S vid)) tf1 ('Just vid : 'Just ('S vid) ': h) ('Nothing ': 'Nothing : o3) c)->
         repr vid tf i3 o3 c
    letSS xy f = 
      letStar' xy $ \x y -> f (x,y)
-}

-- instance (LLC repr,
--           TensP repr ab vio abpair ('S ('S vid)) vido ('Nothing ': 'Just ('S vid) ': h) hout ('Nothing ': 'Nothing ': o4) o4out (consumeC :: Constraint)) =>
--          TensP repr
--          (ab * Qubit)
--          (repr v1 'False i1 o1 : vio)
--          (abpair,repr v1 'False i1 o1 Qubit)
--          vid
--          vido
--          h
--          hout
--          o4
--          o4out
--          (consumeC, Consume ('S vid) i1 o1)
--   where
--   letSS ::
--     (LLC repr, Or tf0 tf1 tf, VarOk tf1 'Nothing,
--      consumeC, Consume ('S vid) i1 o1) =>
--     repr vid tf0 i4 h (ab * Qubit) ->
--     ((abpair,repr v1 'False i1 o1 Qubit) -> repr vido tf1 hout o4out c) ->
--     repr vid tf i4 o4 c
--   letSS xyz f = 
--     letStar' xyz $ \xy z -> letSS xy $ \xy -> f (xy,z)
    


plus_minus :: (LinearQuipper repr) => UVar repr (Bool -> Circ Qubit)
plus_minus = qdefn $ ulam $ \b -> do
  q <- qinit $$ b 
  r <- hadamard ^ q 
  return r

-- simpletest :: (LinearQuipper repr) => UVar repr (Qubit -<> Circ Qubit)
-- simpletest = qdefn $ llam $ \q -> (qdefn hadamard) ^ q

-- simpletest2 :: (LinearQuipper repr) => UVar repr (Qubit -<> Circ Qubit)
-- simpletest2 = qdefn hadamard

share :: LinearQuipper repr => UVar repr (Qubit -<> Circ (Qubit * Qubit))
share = qdefn $ llam' $ \a -> do
  b <- qinit $$ false
  b <- qnot ^ b `controlled` a
  return (a * b)  

bell00 :: LinearQuipper repr => UVar repr (Circ(Qubit * Qubit))
bell00 = qdefn $ do
  a <- plus_minus $$ false
  ab <- share ^ a
  return ab

alice :: LinearQuipper repr => UVar repr (Qubit -<> Qubit -<> Circ (Bang Bit * Bang Bit))
alice = qdefn $ llam' $ \q -> llam' $ \a -> do
  a <- qnot ^ a `controlled` q
  q1 <- hadamard ^ q
  xy <- measure ^ (q1 * a)
  return xy 

bob :: LinearQuipper repr => UVar repr (Qubit -<> (Bit -> Bit -> Circ(Qubit)))
bob = qdefn $ llam' $ \b -> ulam $ \x -> ulam $ \y -> do
  b <- gate_X ^ b `controlled` y
  b <- gate_Z ^ b `controlled` x
  cdiscard $$ (x * y)
  return b

{-
teleport :: Qubit -> Circ Qubit
teleport q = do
  (a,b) <- bell00
  (x,y) <- alice  q  a
  bob  b x y
-}
teleport :: LinearQuipper repr => UVar repr (Qubit -<> Circ Qubit)
teleport =  qdefn $ llam' $ \q -> do
  ab <- bell00
  letSS ab $ \(LV a, LV  b) -> do
    xy <- alice ^ q ^ a
    letSS xy $ \(UV x, UV y) -> do
      bob ^ b $$ x $$ y

-- teleport_letSSM :: LinearQuipper repr =>
--   UVar repr (Qubit -<> Circ Qubit)
-- teleport_letSSM = qdefn $ llam' $ \q ->
--   letSSM bell00 $ \(LV a, LV b) ->
--   letSSM (alice ^ q ^ a) $ \(UV x, UV y) ->
--   bob ^ b $$ x $$ y


letSSM :: (VarOk tf2 var', Or tf1 tf2 tf, LLC_M m repr,
                 TensP
                   repr
                   ab
                   abpair
                   ('S vid)
                   vido
                   ('Nothing : h)
                   hout
                   (var' : o)
                   oout) =>
                repr vid tf1 i h (m ab)
                -> (abpair -> repr vido tf2 hout oout (m b))
                -> repr vid tf i o (m b)
letSSM m k = m `bind_` \a -> letSS a k

sim_teleport = sim_generic 0.123 (convert teleport)

qnothad :: LinearQuipper repr => UVar repr (Qubit -<> Circ Qubit)
qnothad = qdefn $ llam $ \q -> do
  q' <- qnot ^ q
  hadamard ^ q'

sim_qnothad = sim_generic 0.1234 (convert qnothad)

--print_qhadnot = Q.print_simple Q.Preview (convert qnothad)
