
{-#
LANGUAGE
ConstraintKinds,
DataKinds,
FlexibleContexts,
FlexibleInstances,
FunctionalDependencies,
KindSignatures,
MultiParamTypeClasses,
NoMonomorphismRestriction,
PolyKinds,
RankNTypes,
TypeFamilies,
TypeOperators,
UndecidableInstances
#-}

import LLC
import Prelude hiding ((^), (*))

examLLC :: LLC repr => UVar
  repr ((c1 -<> c2) -> (u1 -> u2) -> u1 -> (c1 * d) -<>
        (c2 * d * u2))
examLLC = ulam $ \f -> ulam $ \g ->
          ulam $ \u -> llam $ \cd ->
  let u' = g $$ u in
  letStar cd $ \c d -> ((f ^ c) * d * u')
