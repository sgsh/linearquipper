-- // LinearQuipper templete
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE ConstraintKinds #-}
-- {-# LANGUAGE AllowAmbiguousTypes #-}
 {-# LANGUAGE TypeFamilyDependencies #-}
-- {-# LANGUAGE ScopedTypeVariables #-}
-- {-# LANGUAGE NoMonoLocalBinds #-}
-- {-# LANGUAGE ExistentialQuantification #-}
-- {-# OPTIONS_GHC -freduction-depth=80 #-}
{-# OPTIONS_GHC -fno-defer-type-errors #-}
{-# OPTIONS_GHC -fno-defer-out-of-scope-variables #-}
{-# OPTIONS_GHC -fno-defer-typed-holes #-}
-- {-# OPTIONS_GHC -Wall #-}

import Prelude hiding ((*),(+),(^),(>>=),return,(>>))
import MLL.Core
import MLL.Monad hiding ((>>))
import MLL.Pattern
import Quipper (Circ, Qubit, Bit)
import LinearQuipper
import LinearQuipper.MLL
import GHC.Exts (Constraint)

import QuipperLib.Simulation
import Quipper (print_simple, Format(..))
----------------------------------------------------------------------------
(>>=) = bind_
return = return_
(>>) m1 m2 = m1 >>= \x -> letOne x m2
----------------------------------------------------------------------------

plus_minus :: (LinearQuipper repr) =>
  UVar repr (Bool -> Circ Qubit)
plus_minus = qdefn $ ulam $ \b -> do
  q <- qinit $$ b 
  hadamard ^ q 

share :: LinearQuipper repr =>
  UVar repr (Qubit -<> Circ (Qubit * Qubit))
share = qdefn $llam $ \a -> do
  b <- qinit $$ false
  b <- qnot ^ b `controlled` a
  return (a * b)  

bell00 :: LinearQuipper repr =>
  UVar repr (Circ(Qubit * Qubit))
bell00 = qdefn $ do
  a <- plus_minus $$ false
  share ^ a
  
alice :: LinearQuipper repr =>
  UVar repr (Qubit -<> Qubit -<> Circ (Bang Bit * Bang Bit))
alice = qdefn $ llam $ \q -> llam $ \a -> do
  a <- qnot ^ a `controlled` q
  q1 <- hadamard ^ q
  measure ^ (q1 * a)
  
-- bob :: LinearQuipper repr =>
--   UVar repr (Qubit -<> (Bit -> Bit -> Circ Qubit))
-- bob = qdefn $llam $ \b -> ulam $ \x -> ulam $ \y -> do
--   b <- gate_X ^ b `controlled` y
--   b <- gate_Z ^ b `controlled` x
--   cdiscard $$ (x * y)  
--   return b

bob :: LinearQuipper repr =>
  UVar repr (Qubit -<> ((Bang Bit * Bang Bit) -<> Circ Qubit))
bob = qdefn $ llam $ \b -> llam $ \xy ->
  letSS xy $ \(UV x, UV y) -> do
  LV b' <- gate_X ^ b `controlled` y
  LV b'' <- gate_Z ^ b' `controlled` x
  cdiscard $$ (x * y)
  -- cdiscard $$ x
  -- cdiscard $$ y
  return b''
  where
    (>>=) = letSSM

-- bob :: LinearQuipper repr =>
--   UVar repr (Qubit -<> (Bang (Bit * Bit) -> Circ Qubit))
-- bob = qdefn $llam $ \b -> ulam $ \xy -> letBang xy $ \xy -> letStar xy $ \x y -> do
--   b' <- gate_X ^ b `controlled` y
--   b'' <- gate_Z ^ b' `controlled` x
--     letBang xy $ \xy -> letStar xy $ \x y -> do 
--     cdiscard $$ (x * y)  
--   return b''
      

-- teleportLetSS :: LinearQuipper repr =>
--   UVar repr (Qubit -<> Circ Qubit)
-- teleportLetSS = qdefn $ llam $ \q ->
--   letSSM bell00 $ \(LV a, LV b) ->
--   letSSM (alice ^ q ^ a) $ \(UV x, UV y) ->
--   bob ^ b $$ x $$ y


teleport_lin :: LinearQuipper repr =>
  UVar repr (Qubit -<> Circ Qubit)
teleport_lin = qdefn $ llam $ \q -> do
  (LV a, LV b) <- bell00 
  (UV x, UV y) <- alice ^ q ^ a
  bob ^ b ^ (bang x * bang y)
  where
    (>>=) = letSSM


challenge_letSSM1 :: LinearQuipper repr =>
  UVar repr (Qubit -<> Circ (Qubit * Qubit * Qubit))
challenge_letSSM1 = qdefn $ llam $ \q -> do --llam $ \q2 -> do
  (LV a, LV b) <- bell00
  LV q' <- hadamard ^ q
  return ((a * b) * q')
  where
    (>>=) = letSSM

ccnot :: LinearQuipper repr =>
  UVar repr (Qubit -<> Qubit -<> Qubit -<> Circ(Qubit * Qubit *  Qubit))
ccnot =  qdefn $ llam $ \c -> llam $ \a -> llam $ \b -> do
  c <- qnot ^ c `controlled` (a * b)
  return ((c * a) * b)
  
-- challenge_letSSM2 :: LinearQuipper repr =>
--   UVar repr (Qubit -<> Circ (Bang (Bit * Bit) * Qubit))
-- challenge_letSSM2 = qdefn $ llam $ \q -> do
--   (LV a, LV b) <- bell00
--   LV q' <- hadamard ^ q
--   ((LV q'', LV a'), LV b') <- ccnot ^ q' ^ a ^ b
--   (UV a'', UV b'') <- measure ^ (a' * b')
--   cdiscard $$ (a'' * b'')
--   return (bang (a'' * b'') * q'')
--   where
--     (>>=) = letSSM

hadmeas :: LinearQuipper repr =>
  UVar repr (Qubit -<> Circ (Bang Bit))
hadmeas = llam $ \q -> do
  q' <- hadamard ^ q
  q'' <- measure ^ q'
  return q''
  
-- letSSM m k = m `bind_` \a -> letSS a k

-- badcirc :: LinearQuipper repr =>
--   UVar repr (Qubit -<> Qubit -<> Circ Qubit)
-- badcirc = qdefn $ llam $ \q1 -> llam $ \q2 -> do
--   UV q1' <- measure ^ q1
--   LV q1'' <- hadamard ^ q1
--   return q1''
--   where
--     (>>=) = letSSM

-- badcirc' :: LinearQuipper repr =>
--   UVar repr (Qubit -<> Qubit -<> Circ Qubit)
-- badcirc' = qdefn $ llam $ \q1 -> llam $ \q2 -> do
--   q1' <- measure ^ q1
--   q1'' <- hadamard ^ q1
--   return q1''

    
teleport_old :: LinearQuipper repr =>
  UVar repr (Qubit -<> Circ Qubit)
teleport_old = qdefn $ llam $ \q -> do
  ab <- bell00
  letStar ab $ \a' b' -> do
    xy <- alice ^ q ^ a'
    bob ^ b' ^ xy

-- badcirc'' :: LinearQuipper repr =>
--   UVar repr (Qubit -<> Qubit -<> Circ (Qubit * Qubit))
-- badcirc'' = qdefn $ llam $ \q1 -> llam $ \q2 -> do
--   UV q1' <- measure ^ q1
--   LV q1'' <- hadamard ^ q1
--   return (q1'' * q2) 
--   where
--     (>>=) = letSSM

